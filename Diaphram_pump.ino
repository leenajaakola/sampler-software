/*
 * Sketch for controlling a diaphram pump
 * 
 * Hardware:
 * 5V Arduino
 * Transistor for controlling high V and current power source from the low current output of the Arduino (digital switch)
 * 12V power source
 * Gardner Denver 1420VP DC Diaphram pump 
 * Wire PWR+ to Blue+
 * Wire Brown- to pin 7
 * Wire PWR- to GND
 * Wire Arduino GND to GND
 * 
 * Author: Leena Jaakola
 * Date created: 01-10-2020
 * 
 * Sources:
 * https://programminginarduino.wordpress.com/2016/03/04/project-10-2/
 */

float pumpet;
unsigned long tstart;
unsigned long tstop;

void setup() {
    pinMode(6, OUTPUT);                               // activate vacuum pump
    digitalWrite(6, LOW);                             // til aktivering af vakuumpumpe
    
    Serial.begin(9600);
    delay(1000);
// ADD A SMALL PUMP TO INDICATE PUMP RUNNING
//    tstart = millis();
//    Serial.println("Diaphram pump test");
//    digitalWrite(7, HIGH);
//    delay(500);
//    digitalWrite(7, LOW);
//    tstop = millis();
//    pumpet = (tstop - tstart);    // ms
//    Serial.print("Pumping time: "); Serial.println(pumpet);


}

void loop() {
  digitalWrite(6, HIGH); // turn on pump 5 seconds
  delay(2000);
  digitalWrite(6, LOW);  // turn off pump 5 seconds
  delay(5000);
}
