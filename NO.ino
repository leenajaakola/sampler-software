/*
  Hardware:
  MKR Board
  Alphasense NO-B4 Nitric Oxide Sensor 4-Electrode

  Reads and logs NO gas concentration
  cite: https://forum.arduino.cc/index.php?topic=376338.0
        https://forum.arduino.cc/index.php?topic=533992.0
        https://zueriluft.ch/makezurich/AAN803.pdf
        https://electronics.stackexchange.com/questions/48842/how-to-read-values-from-a-no2-sensor-from-an-arduino

  Pins: VIN 
        GND
        OP1 = sensor 1 (SN1) working electrode
        OP2 = sensor 2 (SN2) auxiliary electrode

  Author: Leena Jaakola
  Created: 30-07-2020
  
*/

#include <Dps310.h>                                        // Grove high precision barometer library
#define NO_WE_Analog A2                                    // Analog 2
#define NO_Aux_Analog A2                                   // to GND

Dps310 Dps310PressureSensor = Dps310();
float temperature;
float pressure;
int16_t baro; 
float NO_WE;                                               // working electrode
float NO_Aux;                                              // auxiliary electrode
float NO_WE_Real;                                          // adjusted NO
float NO_Aux_Real;                                         // adjusted NO
float NO;                                                  // NO [ppb]    
float temp_n;                                              // temperature correction
float NO_offset_WE = 0.3623;  //0.3556;                               // average zero gas for 10 mins (V)
float NO_offset_Aux = 0.3623; //0.3557;                              // average zero gas for 10 mins (V)
float NO_Sensitivity = 0.6750;                             // (100 nA/ppb = 1 mV/ppm) 0.5-0.85 calibration constant; 

void setup() {
  Serial.begin(9600);                                      // Initialize serial monitor

  Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                     // Default I2C address 0x76

  delay(100);
}

void loop() {

  baro = Dps310PressureSensor.measureTempOnce(temperature);
  baro = Dps310PressureSensor.measurePressureOnce(pressure);

  if(temperature <= 20.0) {                                            // temperature correction
    temp_n = 1.04;
  }
  if(20.0 < temperature <= 30.0) {
    temp_n = 1.82;
  }
  else{
    temp_n = 2.00;
  }

  NO_WE = map(analogRead(NO_WE_Analog), 0, 1023, 0, 3300);
  NO_Aux = map(analogRead(NO_Aux_Analog), 0, 1023, 0, 3300);
  
  NO_WE_Real = (NO_WE / 1000) - NO_offset_WE;                          
  NO_Aux_Real = temp_n * ((NO_Aux / 1000) - NO_offset_Aux);            // WE/AE current ratio
  NO = (NO_WE_Real - NO_Aux_Real) / NO_Sensitivity;
  
  Serial.print(NO_WE); Serial.print("\t"); Serial.print(NO_Aux); Serial.print("\t"); Serial.println(NO, 3);
  
  delay(1000);
}
