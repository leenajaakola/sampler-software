/*
 * Teros10 Moisture sensors
 * 
 * Wishlist: 
 * loop 2 sensors 
 * 
 * Author: Leena Jaakola
 * Date created: 02-11-2020
 */

const int dry = 225;              // unicorn                            // Calibrated 2 mins in dry soil
const int sat = 460;              // unicorn                            // Calibrated 2 mins in saturated soil
const int dry2 = 226;             // cherry
const int sat2 = 480;             // cherry
const int moistureAnalogIn = A0;                                  // TEROS10 unicorn
const int moistureAnalogIn2 = A1;                                 // TEROS10 cherry
int moistureAnalog = 0;
int moistureAnalog2 = 0;
int moisturemV;   
int moisturemV2;                                       
float vwc;
float vwc2;

void setup() {
  Serial.begin(9600);
}

void loop() {
  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, dry, sat, 1000, 2500);                      // ********** Map analog output to 1000-2500 mV
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3)                              // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;   

  moistureAnalog2 = analogRead(moistureAnalogIn2);
  moisturemV2 = map(moistureAnalog2, dry2, sat2, 1000, 2500);                  // ********** Map analog output to 1000-2500 mV
  vwc2 = 4.824 * pow(10, -10) * pow(moisturemV2, 3)                            // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV2, 2) 
    + 3.898 * pow(10, -3) * moisturemV2 - 2.154; 


Serial.print(moistureAnalog); Serial.print("\t"); Serial.print(moisturemV); Serial.print("\t"); Serial.print(vwc, 3); Serial.print("\t");
Serial.print(moistureAnalog2); Serial.print("\t"); Serial.print(moisturemV2); Serial.print("\t"); Serial.println(vwc2, 3); 
//Serial.println();
delay(1000);
}
