/* 
 *  Logs data for all parts for hydro loop v.1 EXCEPT TEROS32 Soil potential sensors
 *  
 *  Arduino Mega Board
 *  RG-15 (Pin 2 supports interrupts)
 *  Teros10
 *  Dps310 Grove high precision barometer
 *  Seeed Studio Grove RTC
 *  Sparkfun Level shifting SD
 *  
 *  Author: Leena Jaakola
 *  Date: 05-11-2020
 *  
*/

// CHANGE FILE NAME on SD Card // 
String logFile = "log-0007.txt";

#include <SD.h>
#include <Dps310.h>    
#include <DS1307.h> 
 
#define RG11_Pin 2
const int chipSelect = 53;      
const int moistureAnalogIn = A0;                                  // TEROS10 unicorn
const int moistureAnalogIn2 = A1;                                 // TEROS10 cherry

DS1307 clock; 
const int dry = 225;                                              // unicorn Calibrated 2 mins in dry soil
const int sat = 460;                                              // unicorn Calibrated 2 mins in saturated soil
const int dry2 = 226;                                             // cherry Calibrated 2 mins in dry soil
const int sat2 = 480;                                             // cherry Calibrated 2 mins in saturated soil
int moistureAnalog;
int moistureAnalog2;
int moisturemV;   
int moisturemV2;                                       
float vwc;
float vwc2;
#define Bucket_Size 0.01
volatile unsigned long tipCount = 0;     // bucket tip counter used in interrupt routine
volatile unsigned long ContactTime;  // Timer to manage any contact bounce in interrupt routine
long lastCount = 0;
float totalRainfall = 0;
bool SDAvailable = false;
Dps310 Dps310PressureSensor = Dps310();
float a_temp;
float b_pressure;
uint8_t oversampling = 3;                                 // oversampling can be a value from 0 to 7, allows higher precision
int16_t ret;  
  
void setup() {
   Serial.begin(9600);
   clock.begin();
  
   SDAvailable = SD.begin(chipSelect);                             // initialize SD card:
   Serial.println("Card Initialized: " + String(SDAvailable));
   delay(100);
   Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
   Serial.println("Barometer Initialized");
   File dataFile = SD.open(logFile, FILE_WRITE);
   if (dataFile) {
   dataFile.println("Date Time, No. Tips, Total Rainfall, Air Temperature, Barometric Pressure, VWC_U, VWC_C");
   dataFile.close();
   }
   Serial.print("Hydreon RG-15 Rain Sensor Bucket Size: "); Serial.print(Bucket_Size); Serial.println(" mm");
   pinMode(RG11_Pin, INPUT);   
   attachInterrupt(digitalPinToInterrupt(RG11_Pin), rgisr, FALLING);     // attach interrupt handler to input pin.
   interrupts(); 
}

void loop() {
  clock.getTime();
  char dateTime[40];
  sprintf(dateTime, "%04d/%02d/%02d %02d:%02d:%02d", clock.year + 2000, clock.month, clock.dayOfMonth, clock.hour, clock.minute, clock.second); 

  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);
  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, dry, sat, 1000, 2500);
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3)                              // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;   

  moistureAnalog2 = analogRead(moistureAnalogIn2);
  moisturemV2 = map(moistureAnalog2, dry2, sat2, 1000, 2500); 
  vwc2 = 4.824 * pow(10, -10) * pow(moisturemV2, 3)                            // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV2, 2) 
    + 3.898 * pow(10, -3) * moisturemV2 - 2.154; 
    
  Serial.print(dateTime); Serial.print(","); 
  Serial.print(tipCount); Serial.print(","); 
  Serial.print(totalRainfall); Serial.print(","); 
  Serial.print(a_temp); Serial.print(","); 
  Serial.print(b_pressure); Serial.print(","); 
  Serial.print(vwc); Serial.print(","); 
  Serial.println(vwc2); 

  if (SDAvailable) {
    File dataFile = SD.open(logFile, FILE_WRITE);
    dataFile.println();
    dataFile.print(dateTime); dataFile.print(",");
    dataFile.print(tipCount); dataFile.print(","); dataFile.print(totalRainfall); dataFile.print(","); 
    dataFile.print(a_temp); dataFile.print(","); dataFile.print(b_pressure); dataFile.print(",");
    dataFile.print(vwc); dataFile.print(","); dataFile.print(vwc2);
    dataFile.close();
   }
   
  delay(5000);

  
  noInterrupts(); 
  if(tipCount != lastCount) {
      lastCount = tipCount;
      totalRainfall = tipCount * Bucket_Size;
//      Serial.print("Tip Count: "); Serial.print(tipCount);
//      Serial.print("\tTotal Rainfall: "); Serial.println(totalRainfall); 
   }
   interrupts(); 
}

 
void rgisr ()   { 
   if ((millis() - ContactTime) > 15 ) {
      tipCount++;
      ContactTime = millis();
   } 
} 
