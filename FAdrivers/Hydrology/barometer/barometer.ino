/*
  Hardware:
  MKRZero
  DPS310 High Precision Barometer Pressure and Altitude Sensor

  https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino

  Author: Leena Jaakola
  
*/

#include <Dps310.h>                                         // Grove high precision barometer library

Dps310 Dps310PressureSensor = Dps310();

void setup() {
  Serial.begin(9600);                                       // Initialize serial monitor
  while (!Serial);                            
  
  Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                     // Default I2C address 0x76
  
}

void loop() {
  float temperature;
  float pressure;
  uint8_t oversampling = 7;                                 // oversampling can be a value from 0 to 7, allows higher precision
  int16_t ret;                                              
  Serial.println();
  
  ret = Dps310PressureSensor.measureTempOnce(temperature, oversampling);        // ret = Dps310PressureSensor.measureTempOnce(temperature); DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision

  if (ret != 0)
  {
    Serial.print("Something went wrong. ret = ");
    Serial.println(ret);
  }
  else
  {
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" degrees of Celsius");
  }

  ret = Dps310PressureSensor.measurePressureOnce(pressure, oversampling);       // ret = Dps310PressureSensor.measurePressureOnce(pressure);
  if (ret != 0)
  {
    Serial.print("Something went wrong. ret = ");
    Serial.println(ret);
  }
  else
  {
    Serial.print("Pressure: ");
    Serial.print(pressure);
    Serial.println(" Pascal");
  }

  //Wait some time
  delay(500);
  delay(1000);
}
