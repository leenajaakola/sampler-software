//#include <DS1307RTC.h>

#include <DS1307.h>                                          // Grove RTC
DS1307 clock; 

void setup() {
  Serial.begin(9600);
  clock.begin();
//  clock.fillByYMD(2020,8,10);                              // SET DATE AND TIME or buy a CR1225 battery :) 
//  clock.fillByHMS(12,00,00);
//  clock.setTime();                                         //write time to the RTC chip
}

void loop() {
  clock.getTime();
  char dateTime[40];
  sprintf(dateTime, "%04d/%02d/%02d %02d:%02d:%02d", clock.year + 2000, clock.month, clock.dayOfMonth, clock.hour, clock.minute, clock.second); 

Serial.println(dateTime);
delay(1000);
}
