/*  
 *  Logs data for all parts for hydro loop v.1 EXCEPT RG-15 Rain Sensor
 *   
 *  Arduino Mega Board
 *  Teros32 (Pin 11 supports SDI)
 *  Teros10
 *  Dps310 Grove high precision barometer
 *  Seeed Studio Grove RTC
 *  Sparkfun Level shifting SD
 *  
 *  Author: Leena Jaakola
 *  Date: 04-11-2020
 * 
 */

// CHANGE FILE NAME on SD Card // 
String logFile = "log-0001.txt";
 
#include <Dps310.h>                                               // Call Libraries
#include <SD.h>
#include <DS1307.h>
#include <SDI12.h>

// Assign Pins
const int chipSelect = 53;                                        // SD Card
const int moistureAnalogIn = A0;                                  // TEROS10 unicorn
const int moistureAnalogIn2 = A1;                                 // TEROS10 cherry
#define DATA_PIN 11     
#define Bucket_Size 0.01                                          // bucket size to trigger tip count (mm - DIP 1 set to 0, DIP 2 set to 0)
#define RG15_Pin 2                                                // RG15 connected to digital pin (2 Mega, 6 MKR)

// Define variables 
DS1307 clock; 
bool SDAvailable = false;
Dps310 Dps310PressureSensor = Dps310();
float a_temp;
float b_pressure; 
uint8_t oversampling = 3;                                         // oversampling (0 - 7) increases precision
uint16_t ret;
const int dry = 225;                                              // unicorn Calibrated 2 mins in dry soil
const int sat = 460;                                              // unicorn Calibrated 2 mins in saturated soil
const int dry2 = 226;                                             // cherry Calibrated 2 mins in dry soil
const int sat2 = 480;                                             // cherry Calibrated 2 mins in saturated soil
int moistureAnalog;
int moistureAnalog2;
int moisturemV;   
int moisturemV2;                                       
float vwc;
float vwc2;
String sdiString = "";
const int sdi_sensors = 2;                                        // input number of SDI sensors
SDI12 mySDI12(DATA_PIN);
uint8_t numSensors = 0;
String dataString = "";
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000};
volatile unsigned long tipCount = 0;                              // bucket tip counter used in interrupt routine
volatile unsigned long contactTime = 0;                           // Timer to manage any contact bounce in interrupt routine
long lastCount = 0;
float totalRainfall = 0;

void setup() {
  
  Serial.begin(115200);
  Dps310PressureSensor.begin(Wire);                               // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                           // Default I2C address 0x76
  delay(100);
  SDAvailable = SD.begin(chipSelect);                             // initialize SD card:
  Serial.println("Card working: " + String(SDAvailable));
  delay(100);
  Serial.println("Opening SDI-12 bus");
  mySDI12.begin();
  delay(100);

  Serial.println("Scanning all addresses, please wait...");
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }
  if (!found) {
    Serial.println(
      "No SDI sensors found");
    while (true) {delay(10); }
  }
  
  File dataFile = SD.open(logFile, FILE_WRITE);                       // print a header to the SD card file:
  if (dataFile) {
  dataFile.println("Time Stamp, Temp [C], T10_U θ, T10_C θ, Ψa [Pa], T32_462 Ψs [Pa], T32_462 Soil Temp [C], T32_462 meta, T32_422 Ψs [Pa], T32_422 Soil Temp [C], T32_422 meta");
  dataFile.close();
  }
  Serial.print("Time Stamp"); Serial.print(","); 
  Serial.print("Temp [C]"); Serial.print(","); 
  Serial.print("T10_U VWC θ"); Serial.print(","); 
  Serial.print("T10_C VWC θ"); Serial.print(","); 
  Serial.print("Barometric Pressure Ψa [Pa]"); Serial.print(","); 
  Serial.print("T32_462 Suction Ψs [Pa]"); Serial.print(","); 
  Serial.print("T32_462 Soil Temp [C]"); Serial.print(","); 
  Serial.print("T32_462 meta"); Serial.print(","); 
  Serial.print("T32_422 Suction Ψs [Pa]"); Serial.print(","); 
  Serial.print("T32_422 Soil Temp [C]"); Serial.print(","); 
  Serial.println("T32_422 meta");
  
  pinMode(RG15_Pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(RG15_Pin), isr_rg, FALLING);  
  interrupts();                                                   // Enable Interrupts
}

void loop() {
  
  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);            // DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision
  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);

  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, dry, sat, 1000, 2500);
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3)                              // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;   

  moistureAnalog2 = analogRead(moistureAnalogIn2);
  moisturemV2 = map(moistureAnalog2, dry2, sat2, 1000, 2500); 
  vwc2 = 4.824 * pow(10, -10) * pow(moisturemV2, 3)                            // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV2, 2) 
    + 3.898 * pow(10, -3) * moisturemV2 - 2.154; 

  for(int i = 0; i = sdi_sensors; i++) {
    Serial.print(a_temp); Serial.print(","); 
    Serial.print(vwc); Serial.print(","); 
    Serial.print(vwc2); Serial.print(","); 
    Serial.print(b_pressure);

    if (SDAvailable) {
      File dataFile = SD.open(logFile, FILE_WRITE);
      dataFile.println();
      dataFile.print("\t"); dataFile.print(",");
      dataFile.print(a_temp); dataFile.print(","); 
      dataFile.print(vwc); dataFile.print(","); 
      dataFile.print(vwc2); dataFile.print(","); 
      dataFile.print(b_pressure);
      dataFile.close();
    } 
    for (char i = '0'; i <= '9'; i++)                                           // scan address space 0-9
      if (isTaken(i)) {
      takeMeasurement(i); 
      }
    Serial.println();
  }

  

  noInterrupts();                                              // Disable interrupts
  if(tipCount != lastCount) {                                  
    lastCount = tipCount;
    totalRainfall = tipCount * Bucket_Size;
    Serial.print(tipCount); Serial.print(",");
    Serial.print(totalRainfall); Serial.print(" mm");
      if (SDAvailable) {
        File dataFile = SD.open(logFile, FILE_WRITE);
        dataFile.println();
        dataFile.print("\t"); dataFile.print(",");
        dataFile.close();
      }
  }

  interrupts();    // Enable Interrupts      
  delay(5000);
}

//////////////////////
// Functions ////////
/////////////////////

void isr_rg() {
  if((millis() - contactTime) > 15 ) {                         // debounce of sensor signal
    tipCount++;
    contactTime = millis();
  }
}
  
byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    delay(50);
  }
  String dataString = String(buffer);                                  // save SDI data to a datastring
  Serial.print(dataString);
  if (SDAvailable) {
  File dataFile = SD.open(logFile, FILE_WRITE);
  dataFile.print(dataString); dataFile.print(","); 
  dataFile.close();
  }
}

void printInfo(char i) {                                               // i is a character between '0'-'9', 'a'-'z', or 'A'-'Z'
  String command = "";
  command += (char)i;
  command += "I!";
  mySDI12.sendCommand(command);
  delay(30);
  printBufferToScreen();
}

bool takeMeasurement(char i) {
  String command = "";
  command += i;
  command += "M!";                                                      // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  delay(30);

  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  sdiResponse = String(sdiResponse);  
  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available())                                            // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  delay(30);
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out

  command = "";
  command += i;
  command += "D0!";                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}     
  delay(300);                               
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "0!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
