/*  RG15 tipping bucket
 *  Senses rainfall as a switch and prints total accumulated rainfall 
 *  
 *  Hardware: Rain Gauge Model RG-15
 *  Set dip switches: 1 (1 = inch, 0 = mm)
 *                2 (0 = 0.2mm, 1 = 0.02mm resolution)
 *  Pins 2, 3, 18, 19, 20, 21 on Mega Board support interrupts
 *  (MKR boards: 0, 1, 4, 5, 6, 7, 8, 9, A1, A2)
 *  
 *  Author: Leena Jaakola
 *  Date: 29-09-2020
 *  
 * Sources:
 * http://cactus.io/hookups/weather/rain/hydreon/hookup-arduino-to-hydreon-rg-11-rain-sensor
 * https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/?setlang=it
 * 
 */

#define Bucket_Size 0.2                   // bucket size to trigger tip count (mm - DIP 1 set to 0, DIP 2 set to 0)
#define RG15_Pin 2                        // RG15 connected to digital pin (2 Mega, 6 MKR)
//#define LED_Pin 4                       // LED to indicate RG-15 on

volatile unsigned long tipCount;          // bucket tip counter used in interrupt routine
volatile unsigned long contactTime;       // Timer to manage any contact bounce in interrupt routine
volatile unsigned long lastCount;         // To update tipping bucket count
volatile float totalRainfall;             // total amount of rainfall detected

void setup() {
lastCount = 0;
tipCount = 0;
totalRainfall = 0;
Serial.begin(115200);
Serial.println("RG15 Sensor Test");
Serial.println("Tip Count \tTotal Rainfall (mm)");
pinMode(RG15_Pin, INPUT);
attachInterrupt(digitalPinToInterrupt(RG15_Pin), isr_rg, FALLING);  // When rain is detected, trigger the interrupt on 5V to GND
interrupts(); //  sei();                                          // Enable Interrupts
}

void loop() {
noInterrupts();  //cli();                                         //Disable interrupts; only display the tip count when it has been incremented by the sensor

  if(tipCount != lastCount) {                           // When RG-15 senses rain 
    lastCount = tipCount;
    totalRainfall = tipCount * Bucket_Size;
    Serial.print(tipCount); Serial.print("\t");
    Serial.print(totalRainfall); Serial.println(" mm");
  }
  
interrupts();  //  sei();                                        // Enable Interrupts
}

// Interrupt handler routine that is triggered when the RG15 detects rain
  void isr_rg() {
  if((millis() - contactTime) > 15 ) {                  // debounce of sensor signal
    tipCount++;
    contactTime = millis();
  }
}
