
/*
    Logs data for all parts for hydro loop v.1 EXCEPT TEROS32 Soil potential sensors

    Arduino Mega Board
    RG-15 (Pin 2 supports interrupts)
    Teros10
    Dps310 Grove high precision barometer
    Seeed Studio Grove RTC
    Sparkfun Level shifting SD

    RTC doesnt change
    total rainfall doesnt increase -- total rainfall = tippingbucket * 0.01mm
    Low Power

    Author: Leena Jaakola
    Date: 05-11-2020

*/

/////////////////////
// ** FILE NAME ** //
/////////////////////

String logFile = "log-0001.txt";

#include <SD.h>
#include <Dps310.h>
#include <DS1307.h>
#include <SDI12.h>
#include <LowPower.h>

#define RG11_Pin 2
#define DATA_PIN 11
const int chipSelect = 53;
const int moistureAnalogIn = A0;                                  // TEROS10 unicorn 13081
const int moistureAnalogIn2 = A1;                                 // TEROS10 cherry 13092

DS1307 clock;
char dateTime[40];
const int dry = 225;                                              // unicorn Calibrated 2 mins in dry soil
const int sat = 460;                                              // unicorn Calibrated 2 mins in saturated soil
const int dry2 = 226;                                             // cherry Calibrated 2 mins in dry soil
const int sat2 = 480;                                             // cherry Calibrated 2 mins in saturated soil
int moistureAnalog;
int moistureAnalog2;
int moisturemV;
int moisturemV2;
float vwc;
float vwc2;
#define Bucket_Size 0.01
volatile unsigned long tipCount = 0;     // bucket tip counter used in interrupt routine
volatile unsigned long ContactTime;  // Timer to manage any contact bounce in interrupt routine
long lastCount = 0;
float totalRainfall = 0.00;
bool SDAvailable = false;
Dps310 Dps310PressureSensor = Dps310();
float a_temp;
float b_pressure;
uint8_t oversampling = 3;                                 // oversampling can be a value from 0 to 7, allows higher precision
int16_t ret;
String sdiString = "";
const int sdi_sensors = 2;                                        // input number of SDI sensors
SDI12 mySDI12(DATA_PIN);
uint8_t numSensors = 0;
String dataString = "";
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000
                          };

/////////////////////
// ** SET CLOCK ** //
/////////////////////

void setup() {
  clock.begin();
  //clock.fillByYMD(2020,11,6);                              // SET DATE AND TIME
  //clock.fillByHMS(14,11,30);
  //clock.setTime();
  Serial.begin(9600);
  SDAvailable = SD.begin(chipSelect);                             // initialize SD card:
  Serial.println("Card Initialized: " + String(SDAvailable));
  delay(100);
  Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
  Serial.println("Barometer Initialized");
  delay(100);
  Serial.println("Opening SDI-12 bus");
  mySDI12.begin();
  delay(100);

  Serial.println("Scanning all addresses, please wait...");
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }
  if (!found) {
    Serial.println(
      "No SDI sensors found");
    while (true) {
      delay(10);
    }
  }

  File dataFile = SD.open(logFile, FILE_WRITE);
  if (dataFile) {
    dataFile.println();
    dataFile.println("Datetime, Tips, TotalRainfall, AirTemp, Pressure_a, VWC_U, VWC_C, Pressure_s_462, Soil_Temp_462, meta_462, Pressure_s_422, Soil_Temp_422, meta_422");
    dataFile.close();
  }
  Serial.print("Hydreon RG-15 Rain Sensor Bucket Size: "); Serial.print(Bucket_Size); Serial.println(" mm");
  pinMode(RG11_Pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(RG11_Pin), rgisr, FALLING);     // attach interrupt handler to input pin.
  interrupts();
}

void loop() {


  for (int i = 0; i = sdi_sensors; i++) {

    clock.getTime();
    sprintf(dateTime, "%04d/%02d/%02d %02d:%02d:%02d", clock.year + 2000, clock.month, clock.dayOfMonth, clock.hour, clock.minute, clock.second);
    ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);
    ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
    moistureAnalog = analogRead(moistureAnalogIn);
    moisturemV = map(moistureAnalog, dry, sat, 1000, 2500);
    vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3)                              // Third-order calibration equation mV to VWC
          - 2.278 * pow(10, -6) * pow(moisturemV, 2)
          + 3.898 * pow(10, -3) * moisturemV - 2.154;
    moistureAnalog2 = analogRead(moistureAnalogIn2);
    moisturemV2 = map(moistureAnalog2, dry2, sat2, 1000, 2500);
    vwc2 = 4.824 * pow(10, -10) * pow(moisturemV2, 3)                            // Third-order calibration equation mV to VWC
           - 2.278 * pow(10, -6) * pow(moisturemV2, 2)
           + 3.898 * pow(10, -3) * moisturemV2 - 2.154;
    totalRainfall = tipCount * Bucket_Size;

    Serial.print(dateTime); Serial.print(",");
    Serial.print(tipCount); Serial.print(",");
    Serial.print(totalRainfall); Serial.print(",");
    Serial.print(a_temp); Serial.print(",");
    Serial.print(b_pressure); Serial.print(",");
    Serial.print(vwc); Serial.print(",");
    Serial.print(vwc2);

    if (SDAvailable) {
      File dataFile = SD.open(logFile, FILE_WRITE);
      dataFile.println();
      dataFile.print(dateTime); dataFile.print(",");
      dataFile.print(tipCount); dataFile.print(",");
      dataFile.print(totalRainfall); dataFile.print(",");
      dataFile.print(a_temp); dataFile.print(","); dataFile.print(b_pressure); dataFile.print(",");
      dataFile.print(vwc); dataFile.print(","); dataFile.print(vwc2);
      dataFile.close();
    }
    for (char i = '0'; i <= '9'; i++)
      if (isTaken(i)) {
        takeMeasurement(i);
      }
    delay(300000);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    Serial.println();
  }


  noInterrupts();
  if (tipCount != lastCount) {
    lastCount = tipCount;
  }
  interrupts();
}


void rgisr ()   {
  if ((millis() - ContactTime) > 15 ) {
    tipCount++;
    ContactTime = millis();
  }
  // delay(10000);
}

byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    delay(50);
  }
  String dataString = String(buffer);                                  // save SDI data to a datastring
  Serial.print(dataString);
  if (SDAvailable) {
    File dataFile = SD.open(logFile, FILE_WRITE);
    dataFile.print(dataString);
    dataFile.close();
  }
}

void printInfo(char i) {                                               // i is a character between '0'-'9', 'a'-'z', or 'A'-'Z'
  String command = "";
  command += (char)i;
  command += "I!";
  mySDI12.sendCommand(command);
  delay(30);
  printBufferToScreen();
}

bool takeMeasurement(char i) {
  String command = "";
  command += i;
  command += "M!";                                                      // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  delay(30);

  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  sdiResponse = String(sdiResponse);
  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available())                                            // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  delay(30);
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out

  command = "";
  command += i;
  command += "D0!";                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}
  delay(300);
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "0!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
