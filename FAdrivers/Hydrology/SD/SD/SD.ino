/*
 Test SD with Mega board
 
  SCK to 52
  DO = MISO to 50 
  DI = MOSI to 51
  CS to 53
 */

 
#include <SD.h>   
// CHANGE FILE NAME // 
String logFile = "log-0002.txt";
const uint8_t chipSelect = 53;
bool SDAvailable = false;

void setup() {
  Serial.begin(9600);
  
  SDAvailable = SD.begin(chipSelect);                       // initialize SD card:
  Serial.println("Card working: " + String(SDAvailable));
  File dataFile = SD.open(logFile, FILE_WRITE);             // print a header to the SD card file:
  if (dataFile) {
  dataFile.println("Time Stamp, VWC θ [m^3/m^3], Air Temp [C], Ψa [Pa], Ψt [Pa], Ψm [Pa], Soil Temp [C], meta");
  dataFile.close();
  }
}

void loop() {
  if (SDAvailable) {
  File dataFile = SD.open(logFile, FILE_WRITE);
  dataFile.println("hellos");
  dataFile.close();
  
  delay(5000);                
  }

}
