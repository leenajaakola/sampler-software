/* 
  Hardware:
  Arduino Mega/ Uno or MKR board with external power source (5-12V)
  METER group TEROS32 soil water potential sensor, 
  Power source for Teros32 (3.6-28V)
  METER group TEROS10 soil volumetric water content sensor
  DPS310 High Precision Barometer Pressure and Altitude Sensor
  Sparkfun SD level shifting breakout

  Collects data from Teros32, Teros10, DPS310, parses data and
  stores on SD breakout. For SDI signals from 2 TEROS32 soil potential sensors,
  connect to the same pin and change the addresses 
  of the sensors one at a time (ex. 0 and 1) 
  using Examples -> SDI-12 -> b_change_address
  
  Teros10 A0
  Teros32 digital (orange) pin 10
  DPS310 SDA, SCL 
  SD:
  SCK to 52 (9)
  DO = MISO to 50 (10)
  DI = MOSI to 51 (8)
  CS to 53 (4)

  Author:      Leena Jaakola

  Created: 04-10-2020

  Sources:     @file d_simple_logger.ino 
               @file SDI-12 -> b_change_address.ino
               https://www.envirodiy.org/topic/arduino-datalogger/#post-1834
               https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
               https://iot-guider.com/arduino/serial-communication-between-two-arduino-boards/
               https://thewanderingengineer.com/2015/05/06/sending-16-bit-and-32-bit-numbers-with-arduino-i2c/#
               https://forum.arduino.cc/index.php?topic=592795.0

 */

#include <Dps310.h>                                               // Grove high precision barometer library
#include <SDI12.h>
#include <SD.h>   

// CHANGE FILE NAME // 
String logFile = "log-0016.txt";
const uint8_t chipSelect = 4;
bool SDAvailable = false;

Dps310 Dps310PressureSensor = Dps310();
float a_temp;
float b_pressure;
uint8_t oversampling = 3;                                         // oversampling can be a value from 0 to 7, allows higher precision
uint16_t ret;

#define DATA_PIN 6                                               // pin of the SDI bus
String sdiString = "";                                            // save SDI data to a string
SDI12 mySDI12(DATA_PIN);  
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000};
uint8_t numSensors = 0;

void setup() {
  Serial.begin(115200);
  
  SDAvailable = SD.begin(chipSelect);                       // initialize SD card:
  Serial.println("Card working: " + String(SDAvailable));
  File dataFile = SD.open(logFile, FILE_WRITE);             // print a header to the SD card file:
  if (dataFile) {
  dataFile.println("Time Stamp, VWC θ [m^3/m^3], Air Temp [C], Ψa [Pa], Ψt [Pa], Ψm [Pa], Soil Temp [C], meta");
  dataFile.close();
  }
  
  Dps310PressureSensor.begin(Wire);                               // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                           // Default I2C address 0x76

  mySDI12.begin();
  Serial.println("Opening SDI-12 bus");
  delay(500); 
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'a'; i <= 'z'; i++)                               // scan address space a-z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'A'; i <= 'Z'; i++)                               // scan address space A-Z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of SDI sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }

  if (!found) {
    Serial.println("No SDI sensors found");
  }

  Serial.println();
  Serial.println( " Air Temperature [degC], Barometric Pressrue [kPa], T3200462 [kPa], T3200462 [degC], T3200462 meta, T3200466 [kPa], T3200466 [degC], T3200466 meta, ");
}

void loop() {
  
  for (char i = '0'; i <= '9'; i++)                                            // scan address space 0-9
    if (isTaken(i)) {
      printInfo(i);
      takeMeasurement(i);
    }
  for (char i = 'a'; i <= 'z'; i++)                                            // scan address space a-z
    if (isTaken(i)) {
      printInfo(i);
      takeMeasurement(i);
    }

  for (char i = 'A'; i <= 'Z'; i++)                                            // scan address space A-Z
    if (isTaken(i)) {
      printInfo(i);
      takeMeasurement(i);
    };

  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);            // DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision
  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
  b_pressure = b_pressure / 1000;
  
  Serial.print("Air temp: "); Serial.println(a_temp);
  Serial.print("Barometric Pressure: "); Serial.println(b_pressure, 5);
  Serial.print("SDIdata: "); Serial.println(sdiString);
  Serial.println();

  if (SDAvailable) {
  File dataFile = SD.open(logFile, FILE_WRITE);
  dataFile.print(a_temp); dataFile.print(",");
  dataFile.print(b_pressure);
  dataFile.println(sdiString);
  dataFile.close();
  
  delay(5000);                                                                // wait 10 seconds between measurements 
}
delay(5000);
}



//////////////////////
// Functions ////////
/////////////////////

byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    sdiString = String(buffer);                                      // save SDI data to a datastring
    delay(50);
  }
  //Serial.print(buffer);
}

void printInfo(char i) {                                               // i a character between '0'-'9', 'a'-'z', or 'A'-'Z'
  String command = "";
  command += (char)i;
  command += "I!";
  mySDI12.sendCommand(command);
  delay(30);
  printBufferToScreen();
}

bool takeMeasurement(char i) {
  String command = "";
  command += i;
  command += "M!";                                                      // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  delay(30);

  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  //Serial.print(sdiResponse); Serial.print(", "); Serial.print(wait); Serial.print(", ");

  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect
  //Serial.print(numMeasurements); Serial.print(", ");

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available())                                            // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  delay(30);
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out

  command = "";
  command += i;
  command += "D0!";                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}     
  delay(300);                               
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
