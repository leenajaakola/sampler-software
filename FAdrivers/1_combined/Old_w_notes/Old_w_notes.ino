/*
 
Arduino Pro Mini

Missing form Bilag 4: Programmer til Arduino
const int hojde = 320;                                  //løftehøjden i cm fra sugecellen og til overkant af stigrøret

*/



#include "HX711.h"                                      // Arduino library for reading load cells / weight scales - to interface the <a href="http://image.dfrobot.com/image/data/SEN0160/hx711_english.pdf">Avia Semiconductor HX711 24-Bit Analog-to-Digital Converter (ADC)</a>
// #include <HX711.h>                                   // interface the Avia semiconductor HX711 24-Bit Analog-to-Digital Converter (ADC) for Weight Scales
#include <Wire.h>                                       // Allows you to communicate with I2C / TWI devices. On the Mega - 20 (SDA), 21 (SCL)
// #include <I2C_Anything.h>                            // remove // Arduino library to allow sending float, int, or long between Arduino devices https://github.com/nickgammon/I2C_Anything
#include <SPI.h>                                        // Allows you to communicate with serial peripheral interface SPI devices
#include <SD.h>                                         // Allows for reading from and writing to SD cards, e.g. on the Arduino Ethernet Shield.
// #include <LowPower.h>                                // remove // power saving library
#include <ArduinoLowPower.h>                            // Low-Power not available for mkr1400, use ArduinoLowPower
#include <RTCZero.h>                                    // enabled an Arduino to control and use the internal real time clock
#include <Sodaq_DS3231.h>                               // Arduino library for the DS3231 RTC (Real Time Clock)
#include <MKRGSM.h>                                     // Arduino MKR GSM 1400 has a modem that transfers data from a serial port to the GSM network

//calibrate load cell

//#define calibration_factor 395                       //Sampler 1
//#define calibration_factor 419                       //Sampler 2
#define calibration_factor 436                       //Sampler 3          // This value is obtained by using the SparkFun_HX711_Calibration sketch https://github.com/sparkfun/HX711-Load-Cell-Amplifier/blob/master/firmware/SparkFun_HX711_Calibration/SparkFun_HX711_Calibration.ino
//#define calibration_factor 391                       //Sampler 4
//#define calibration_factor 419                       //Sampler 5
//#define calibration_factor 424.0                     //Sampler 6


//float gain = -1.1652; float poffset = 1098.8; // Til sampler 1
//float gain = -1.4516; float poffset = 1291.4; // Til sampler 2
float gain = -1.5077; float poffset = 1424.2; // Til sampler 3  
//float gain = -1.4242; float poffset = 1281.1; // Til sampler 4
//float gain = -1.4099; float poffset = 1262.7; // Til sampler 5
//float gain = -1.3466; float poffset = 1298.6; // Til sampler 6

/*
   Error: just for calibration?
*/
#define DOUT  3
#define CLK  4
//const int LOADCELL_DOUT_PIN = 3;                   //https://github.com/bogde/HX711/blob/0.7.2/examples/HX711_basic_example/HX711_basic_example.ino
//const int LOADCELL_SCK_PIN = 4;
HX711 scale;


const int chipSelect = 10;                           //til SD-kort

float pumpetid;
float pumpet;
unsigned long tstart;
unsigned long tslut;

byte table[8];                                       //til dataoverførsel til MEGA
boolean haveData;

int pressurePin = A2;                                //analog port til tryksensor
int16_t pressureValue;
int16_t vakuumm;
int16_t vakuumvar;
int16_t startpressure;

int vakuum = 110;                                    // startvakuum som der suges med ved sugecellen
int maxw = 800;                                      // Maxwakuum
int16_t thresholdvakuum = vakuum;                    // niveau for hvornår vakuumpumpen skal starte

int levelPin = A0;                                   // analog pin til niveausensor
int levelValue;
const int thresholdlevel = 500;                      // niveau for at magnetventilen skal aktiveres. Magnetventilen står som udgangspunkt på ca. 1023, men ved vandkontakt falder

float loadValue;                                     // load cell 
int16_t loadValueint = 0;

float spanding;
byte spanpin = A3;                                   // analog port til måling af batterispænding
int a;                                               // tæller til det store loop
int b;                                               // tæller til GSM loop
int t = 21;                                          // tæller til at sikre at der samples oftere i starten
long s;                                              // tæller til at sikre, at det kun er i starten der samples ofte
uint32_t old_ts;

float loada = 0;
float loadb = 0;
float loadc = 0;
float loadd = 0;
uint32_t tida = 0;
uint32_t tidb = 0;
uint32_t tidc = 0;
uint32_t tidd = 0;
float dl = 0;
float dt = 0;

float flow;                                         //flow igennem sorbecellen må ikke overstige 0,5 L/døgn
const int flowmax = 16;                             //(384 ml/døgn/(60*60)) omregnet til ml/time
const int flowmin = 8;                              //(192 ml/døgn/(60*60)) omregnet til ml/time

void setup() {
  pinMode(2, OUTPUT);                               //til aktivering af MEGA/GSM-kort og RTC
  pinMode(5, OUTPUT);                               //til aktivering af vægt og niveauswitch
  pinMode(6, OUTPUT);                               //til aktivering af tryksensor
  pinMode(7, OUTPUT);                               //til aktivering af vakuumpumpe
  pinMode(8, OUTPUT);                               //til aktivering af ventil
  pinMode(9, OUTPUT);                               //til aktivering af SDkort
  digitalWrite(2, LOW);                             //til aktivering af MEGA/GSM-kort og RTC
  digitalWrite(5, HIGH);                            //til aktivering af vægt og niveauswitch
  digitalWrite(6, LOW);                             //til aktivering af tryksensor
  digitalWrite(7, LOW);                             //til aktivering af vakuumpumpe
  digitalWrite(8, LOW);                             //til aktivering af ventil
  digitalWrite(9, LOW);                             //til aktivering af SDkort

  Serial.begin(9600);
  
  delay(2000);
  
  // Serial.print("Reading: ");
  
  scale.set_scale(calibration_factor);              
  // scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);  
  scale.tare();                                     // Assuming there is no weight on the scale at start up, reset the scale to 0

  //Serial.print("Reading: ");
  loadValue = (scale.get_units());                  
  // loadValue = scale.read(); 
  Serial.print(loadValue);                          // scale.get_units() returns a float
  Serial.print(" g");                               // You can change this to kg but you'll need to refactor the calibration_factor
  Serial.println();

  delay(1000);

  digitalWrite(6, HIGH);                            // tryksensor
  delay(400); 
  Serial.println("test1");
  delay(400);
  pressureValue = analogRead(pressurePin);
  vakuumvar = (pressureValue * (gain)) + poffset;
  if (pressureValue <= 0) {
    pressureValue = 0;
  }
  //  vakuumvar = 1000 - pressureValue;
  delay(400);
  Serial.print("TresholdP: ");
  Serial.println(thresholdvakuum);
  Serial.print("vakuumvar: ");
  Serial.println(vakuumvar);
  delay(400);
  tstart = millis();
  Serial.println("Test2");
  delay(1000);
  while (vakuumvar < thresholdvakuum)
  {
    //Serial.println("Test3");
    //delay(1000);
    digitalWrite(7, HIGH);
    // Serial.println("Test4");
    //delay(1000);
    delay(30);
    //Serial.println("test5");
    //delay(400);
    //delay(300);
    pressureValue = analogRead(pressurePin);
    vakuumvar = (pressureValue * gain) + poffset;
    if (pressureValue <= 0) {
      pressureValue = 0;
    }
    //Serial.println("test6");
    // vakuumvar = pressureValue;
    // Serial.println(pressureValue);
    Serial.print("TresholdP: ");
    Serial.println(thresholdvakuum);
    Serial.print("vakuumvar: ");
    Serial.println(vakuumvar);
  }
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  tslut = millis();
  pumpet = (tslut - tstart);
  pumpetid = pumpet / 60000;
  Serial.println("pumpetid: ");
  Serial.println(pumpetid);
  Serial.println("Test7");
  delay(1000);
  spanding = ((0.0133 * analogRead(spanpin)) - 0.2564);
  Serial.println("spanding: ");
  Serial.println(spanding);


  Wire.begin(8);                                    // i2c bus kædes sammen med addresse #8
  Wire.onRequest(requestEvent);

  Serial.println("Test8");
  delay(1000);
  digitalWrite(2, HIGH);                            //til aktivering af MEGA/GSM-kort og RTC
  Serial.println("start mega");
  delay(90000);
  Serial.println("slut 90 sek");
  digitalWrite(2, LOW);

  Serial.print("vakuum: ");
  Serial.println(vakuum);

}





void loop() {
  //Serial.print("a: ");
  //Serial.println(a);
  if (a == 2) {
    digitalWrite(6, HIGH);
    delay(500);
    pressureValue = analogRead(pressurePin);
    vakuumvar = (pressureValue * gain) + poffset;
    if (vakuumvar <= 0) {
      vakuumvar = 0;
    }
    thresholdvakuum = vakuum;
    int g = 0;
    tstart = millis();
    while ((vakuumvar < thresholdvakuum) && (g < 200))
    {
      digitalWrite(7, HIGH);
      pressureValue = analogRead(pressurePin);
      delay(30);
      vakuumvar = (pressureValue * gain) + poffset;
      if (vakuumvar <= 0) {
        vakuumvar = 0;
      }
      g = g + 1;
    }
    digitalWrite(7, LOW);
    tslut = millis();
    pumpet = (tslut - tstart);
    pumpetid = pumpetid + (pumpet / 60000);
    delay(500);
    digitalWrite(6, LOW);                          //til deaktivering af tryksensor
  }
   if (a == 4) {
    digitalWrite(6, HIGH);
    delay(500);
    pressureValue = analogRead(pressurePin);
    vakuumvar = (pressureValue * gain) + poffset;
    if (vakuumvar <= 0) {
      vakuumvar = 0;
    }
    thresholdvakuum = vakuum;
    int g = 0;
    tstart = millis();
    while ((vakuumvar < thresholdvakuum) && (g < 200))
    {
      digitalWrite(7, HIGH);
      pressureValue = analogRead(pressurePin);
      delay(30);
      vakuumvar = (pressureValue * gain) + poffset;
      if (vakuumvar <= 0) {
        vakuumvar = 0;
      }
      g = g + 1;
    }
    digitalWrite(7, LOW);
    tslut = millis();
    pumpet = (tslut - tstart);
    pumpetid = pumpetid + (pumpet / 60000);
    delay(500);
    digitalWrite(6, LOW);                          //til deaktivering af tryksensor
  }

  if (a > 6) {
    digitalWrite(5, HIGH);                         //til aktivering af vægt og niveauswitch
    digitalWrite(6, HIGH);                         //til aktivering af tryksensor
    digitalWrite(9, HIGH);                         //til aktivering af SDkort
    delay(500);
    loadValue = 0;

    pressureValue = analogRead(pressurePin);
    vakuumvar = (pressureValue * gain) + poffset;
    if (vakuumvar <= 0) {
      vakuumvar = 0;
    }
    //    vakuumvar = 1000 - pressureValue;
    levelValue = analogRead(levelPin);
    thresholdvakuum = vakuum;
    int g = 0;
    tstart = millis();
    while ((vakuumvar < thresholdvakuum) && (g < 200))
    {
      digitalWrite(7, HIGH);
      pressureValue = analogRead(pressurePin);
      delay(30);
      vakuumvar = (pressureValue * gain) + poffset;
      if (vakuumvar <= 0) {
        vakuumvar = 0;
      }
      //      vakuumvar = 1000 - pressureValue;
      /*Serial.print("TresholdP: ");
        Serial.println(thresholdvakuum);
        Serial.print("vakuumvar: ");
        Serial.println(vakuumvar);*/
      g = g + 1;
    }
    digitalWrite(7, LOW);
    tslut = millis();
    pumpet = (tslut - tstart);
    pumpetid = pumpetid + (pumpet / 60000);
    delay(500);
    digitalWrite(6, LOW);                          //til deaktivering af tryksensor
    /* while (levelValue < thresholdlevel) {
       digitalWrite(8, HIGH);                       //til aktivering af ventil
       levelValue = analogRead(levelPin);
       delay(20000);
      }*/

    loadValue = (scale.get_units());
    digitalWrite(5, LOW);

    spanding = ((0.0133 * analogRead(spanpin)) - 0.2564);
    /* Serial.println("spanding: ");
      Serial.println(spanding);

      Serial.print("vakuumvar: ");
      Serial.println(vakuumvar);

      Serial.print("loadValue: ");
      Serial.println(loadValue);

      Serial.print("levelValue: ");
      Serial.println(levelValue);


      Serial.print("pumpetid: ");
      Serial.println(pumpetid);

      Serial.print("vakuum: ");
      Serial.println(vakuum);
    */
    //data opsamling på google spreadsheet og SD-kort
    if (b > 190 || (t > 20 && t < 30)) {   //før stod der: if (b > 380 || (t > 20 && t < 30)) {
      Serial.println("start mega");
      digitalWrite(2, HIGH);
      delay(90000);
      //Serial.println("slut 90 sek");
      String timestamp;
      Serial.println("trin 1");
      rtc.begin();
      Serial.println("trin 2");
      delay(2000);
      Serial.println("trin 3");
      DateTime now = rtc.now();
      Serial.println("trin 4");
      uint32_t ts = now.getEpoch();
      if (old_ts == 0 || old_ts != ts) {
        old_ts = ts;
        timestamp = String(now.year(), DEC) + "/" + String(now.month(), DEC) + "/" + String(now.date(), DEC) + " " + String(now.hour(), DEC) + ":" + String(now.minute(), DEC) + ":" + String(now.second(), DEC);
        Serial.println(timestamp);
        //finde flow og regulere vakuum
      }
      if (s > 0) {  // Her stod der 380
        if (s < 1140) {
          tidb = now.getEpoch();
          loadb = loadValue;
          dl = loadb - loada;
          dt = tidb - tida;
          tida = tidb;
          loada = loadb;
        }
        if (s > 1140) {
          tidd = now.getEpoch();
          loadd = loadValue;
          dl = loadd - loada;
          dt = tidd - tida;
          tida = tidb;
          loada = loadb;
          tidb = tidc;
          loadb = loadc;
          tidc = tidd;
          loadc = loadd;
        }
        flow = (dl * 3600) / dt;                    //ml/time
        Serial.print(" s: ");
        Serial.print(s);
        Serial.print(" t: ");
        Serial.print(t);
        Serial.print(" b: ");
        Serial.print(b);
        Serial.print(" timestamp: ");
        Serial.print(timestamp);
        Serial.print(" TresholdP: ");
        Serial.print(thresholdvakuum);
        Serial.print(" vakuumvar: ");
        Serial.print(vakuumvar);
        Serial.print(" DL: ");
        Serial.print(dl);
        Serial.print(" DT: ");
        Serial.print(dt);
        Serial.print(" Flow: ");
        Serial.println(flow);
        if (flow < flowmin && vakuum < maxw) {
          vakuum = vakuum + 2;
           = vakuum;
        }
        if (flow > flowmax) {
          vakuum = vakuum - 2;
          thresholdvakuum = vakuum;
        }
      }
      digitalWrite(2, LOW);
      //Serial.println(3);
      //dataopsamling på SD kort
      String dataString = "";
      dataString = timestamp + "," + String(vakuumvar) + "," + String(loadValue) + "," + String(levelValue) + "," + String(vakuum) + "," + String(flow) + "," + String(s) + "," + String(loadd) + "," + String(loada) + "," + String(tidd) + "," + String(tida) + "," + String(dl) + "," + String(dt);
      pinMode(10, OUTPUT);
      SD.begin(chipSelect);
      File dataFile = SD.open("datalog.txt", FILE_WRITE);
      // if the file is available, write to it:
      if (dataFile) {
        dataFile.println(dataString);
        dataFile.close();
        // Serial.print("dataString");
        //Serial.print(dataString);
      }
      b = 0;
      if (s < 300) {
        t = 0;
      }
    }

    digitalWrite(9, LOW);                            //til deaktivering af SDkort
    a = 0;
  }
  a = a + 1;
  b = b + 1;
  t = t + 1;
  s = s + 1;
  delay(1000);
  // LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  LowPower.sleep(8000);         // figure out adc_off and bod_off for ArduinoLowPower
}
// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent()
{
  float span = spanding * 100;
  int16_t spandingint = span;
  int16_t pumpetidint = pumpetid * 100;
  Serial.print("pumpetidint: ");
  Serial.println(pumpetidint);
  vakuumm = vakuumvar;
  loadValueint = loadValue;
  table[0] = (loadValueint >> 8) & 0xFF;
  table[1] = loadValueint & 0xFF;
  table[2] = (vakuumm >> 8) & 0xFF;
  table[3] = (vakuumm) & 0xFF;
  table[4] = (spandingint >> 8) & 0xFF;
  table[5] = (spandingint) & 0xFF;
  table[6] = (pumpetidint >> 8) & 0xFF;
  table[7] = (pumpetidint) & 0xFF;
  Wire.write(table, 8);
}
