/*
  Hardware:
  *MKRZero
  *Alphasense NO-B4 Nitric Oxide Sensor 4-Electrode
  *EDT directIon nitrate and reference flow cells
    (Reference cell must be first in series, flow 
    from bottom to top to eliminate bubbling)
  *DPS310 High Precision Barometer Pressure and Altitude Sensor
  *DS1307 RTC
  *micro SD card

  Reads and logs NO gas concentration
  cite: @file d_simple_logger.ino 
        https://forum.arduino.cc/index.php?topic=376338.0
        https://forum.arduino.cc/index.php?topic=533992.0
        https://zueriluft.ch/makezurich/AAN803.pdf
        https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
        https://www.chem.tamu.edu/class/fyp/stone/tutorialnotefiles/electro/nernst.htm
        https://www.horiba.com/fileadmin/uploads/Scientific/water_quality/Documents/Instruction_Manuals/Electrodes_and_Accessories/2017_Combination_ISEs/GZ0000452163_IM_E_6581S-10C_low.pdf

  NO-B4: VIN, GND, A3, GND < OP1 = sensor 1 (SN1) working electrode, OP2 = sensor 2 (SN2) auxiliary electrode
  Flowcell: red A4, black GND
  DPS310: SCL, SDA, PWR, GND
  DS1307: SCL, SDA, PWR, GND
        

  Author: Leena Jaakola

  Created: 31-07-2020
  
*/

//#include <SD.h>                                          // SD library
#include <Dps310.h>                                        // Grove high precision barometer library
#include <DS1307.h>                                        // Grove RTC

//const int chipSelect = SS1;                              // SD SPI pin MKRZero (should be the same for all MKR and other SAMD boards)  also SDCARD_SS_PIN?
//File dataFile;
DS1307 clock;                                              // object for DS1307 class 
Dps310 Dps310PressureSensor = Dps310();
#define flowcellPin A4                                     // flowcell analog output
#define NO_WE_Analog A3
#define NO_Aux_Analog A3

float a_temp = 20.0;                                       // [degC] air temperature
float b_pressure;                                          // [Pa] barometric pressure
int oversampling = 3;                                      // oversampling can be a value from 0 to 7, allows higher precision
uint16_t baro;

int NO_WE;                                                 // working electrode
int NO_Aux;                                                // auxiliary electrode
float NO_WE_Real;                                          // NO WE
float NO_Aux_Real;                                         // NO Aux
float NO;                                                  // NO [ppb]    
float temp_n;                                              // ppm NO limit of performance warranty
float NO_offset_WE = 0.3556;                               // V average 
float NO_offset_Aux = 0.3557;                              // V average
float NO_Sensitivity = 0.6750;                             // 0.5-0.85 nA/ppb (mV/ppm)

void setup() {
  Serial.begin(9600);                                      // Initialize serial monitor

//  if (!SD.begin(chipSelect)) {                             // Initialize SD card
//   Serial.println("Failed to initialize SD card!");
//   while (1);                                     
//  }
//  File dataFile = SD.open("log-0002.txt", FILE_WRITE);       // Change file name here AND below
//  delay(1000);
//  dataFile.println("Date Time, Pressure [Pa], Air Temp [C], NO_3^- [ppm], NO [ppb]");    
//  dataFile.close();

  clock.begin();

  Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                     // Default I2C address 0x76
  
  delay(100);
}

void loop() {

  clock.getTime();                                                                        // get date and time from DS1307
  char rtcdate[22];
  sprintf(rtcdate, "%04d/%02d/%02d %02d:%02d:%02d", clock.year + 2000, clock.month, clock.dayOfMonth, clock.hour, clock.minute, clock.second);   // convert to character arrays
  
  baro = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);                      // measure air temp and barometric pressure
  baro = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);

  if(a_temp <= 20.0) {
    temp_n = 1.04;
  }
  if(20.0 < a_temp <= 30.0) {
    temp_n = 1.82;
  }
  else{
    temp_n = 2.00;
  }
  
  NO_WE = map(analogRead(NO_WE_Analog), 0, 1023, 0, 3300);                        // measure NO gas working electrode
  NO_Aux = map(analogRead(NO_Aux_Analog), 0, 1023, 0, 3300);                      // auxiliary electrode
  NO_WE_Real = (NO_WE / 1000) - NO_offset_WE;
  NO_Aux_Real = temp_n * ((NO_Aux / 1000) - NO_offset_Aux);                       // WE/AE current ratio (changes with temperature!) Alphasense Application Note AAN 803 Table 3
  NO = (NO_WE_Real - NO_Aux_Real) / NO_Sensitivity;

  Serial.println("Date" "\t" "Time" "\t" "Air Temp [C]" "\t" "Pressure [Pa]" "\t" "NO3 [ppm]" "\t" "NO gas [ppb]");
  Serial.print(rtcdate); Serial.print("\t"); Serial.print(a_temp); Serial.print("\t"); Serial.print(b_pressure); Serial.print("\t"); Serial.print("\t"); Serial.println(NO);
  Serial.println();

  Serial.print("NO_working [mV]: "); Serial.println(NO_WE);
  Serial.print("NO_auxiliary [mV]: "); Serial.println(NO_Aux);
  Serial.print("NO [ppb]: "); Serial.println(NO);
  Serial.println();

//  dataFile = SD.open("log-0002.txt", FILE_WRITE);                       // Print to textfile on SD
//  delay(1000);
//  dataFile.println(String(rtcdate) + "," + String(a_temp) + "," +  String(b_pressure) + "," +  String(no3) + "," +  String(NO));
//  dataFile.close();
  
  delay(3000); 
}
