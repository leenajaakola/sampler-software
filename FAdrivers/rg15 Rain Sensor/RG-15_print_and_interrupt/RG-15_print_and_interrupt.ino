/* This is a demo for the Hydreon Rain Sensor. 

For information about hooking up this board to the Arduino then click on this link 
to visit the hookup guide on the cactus.io website.
http://cactus.io/hookups/weather/rain/hydreon/hookup-arduino-to-hydreon-rg-11-rain-sensor
*/

#define RG11_Pin 2
#define Bucket_Size 0.01
volatile unsigned long tipCount = 0;     // bucket tip counter used in interrupt routine
volatile unsigned long ContactTime;  // Timer to manage any contact bounce in interrupt routine
long lastCount = 0;
float totalRainfall = 0;

#include <Dps310.h>                                         // Grove high precision barometer library
Dps310 Dps310PressureSensor = Dps310();
  float temperature;
  uint8_t oversampling = 7;                                 // oversampling can be a value from 0 to 7, allows higher precision
  int16_t ret;  
  
void setup() {

   Dps310PressureSensor.begin(Wire);                         // Initialize DPS310 default I2C address
   Serial.begin(9600);
   Serial.println("Hydreon RG-15 Rain Sensor Bucket Size: "); Serial.print(Bucket_Size); Serial.println(" mm");
   pinMode(RG11_Pin, INPUT);   // set the digital input pin to input for the RG-11 Sensor
   attachInterrupt(digitalPinToInterrupt(RG11_Pin), rgisr, FALLING);     // attach interrupt handler to input pin.
   // we trigger the interrupt on the voltage falling from 5V to GND

   interrupts(); //sei();         //Enables interrupts
}

void loop() {
    
  ret = Dps310PressureSensor.measureTempOnce(temperature, oversampling);
  Serial.println(temperature);
  delay(1000);
   
   noInterrupts(); //cli();         //Disable interrupts
  
   if(tipCount != lastCount) {
      lastCount = tipCount;
      totalRainfall = tipCount * Bucket_Size;
      Serial.print("Tip Count: "); Serial.print(tipCount);
      Serial.print("\tTotal Rainfall: "); Serial.println(totalRainfall); 
   }
  
   interrupts(); //sei();         //Enables interrupts
}

// Interrrupt handler routine that is triggered when the rg-11 detects rain   
void rgisr ()   { 

   if ((millis() - ContactTime) > 15 ) {  // debounce of sensor signal
      tipCount++;
      ContactTime = millis();
   } 
} 
// end of rg-11 rain detection interrupt handler 
