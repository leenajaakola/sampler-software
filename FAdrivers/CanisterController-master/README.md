# CanisterController
CanisterController for DMR

## Getting Started
The project uses the Arduino MKR GSM 1400 board.

### Getting Started with the Arduino MKR GSM 1400
https://www.arduino.cc/en/Guide/MKRGSM1400
- Tools menu / Boards / Boards Manager / Search "MKR GSM" / Install
- Tools select the Board Arduino MKR GSM 1400

### uRTCLib
- https://github.com/Naguissa/uRTCLib
- Sketch / Include Library / Add .Zip Library / Open libraries folder and select uRTCLib.zip
- Or
- Sketch / Include Library / Manage Libries / Search uRTCLib / Install

### ZEeprom
- https://github.com/zoubworldArduino/ZEeprom
- Sketch / Include Library / Add .Zip Library / Open libraries folder and select Zeeprom.zip
- Or
- Sketch / Include Library / Manage Libries / Search Zeeprom / Install

### MKRGSM
- https://github.com/arduino-libraries/MKRGSM
- Sketch / Include Library / Add .Zip Library / Open libraries folder and select MKRGSM.zip
- Or
- Sketch / Include Library / Manage Libries / Search MKRGSM / Install

## Git stuff
- $ git config --global core.autocrlf true (Configure Git to ensure line endings in files you checkout are correct for Windows.)
- $ eval $(ssh-agent -s)
- $ ssh-add ~/.ssh/tipoca99_github OR ssh-add ~/.ssh/github
- Add to ~/.gitconfig
[alias]
    st = status
    ci = commit
    br = branch
    co = checkout
    df = diff
    lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
    lga = log --all --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'

## Visual Studio Code extension for Arduino
- https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.vscode-arduino
- To make Intelli Sense Engine (no red lines when variable is defined in another ino file):
  - File/Preferences/settings
  - search intellisense
  - Set: S_cpp: Intelli Sense Engine: Tag Parser

## warning: "LITTLE_ENDIAN" redefined
- https://forum.arduino.cc/index.php?topic=634821.0
