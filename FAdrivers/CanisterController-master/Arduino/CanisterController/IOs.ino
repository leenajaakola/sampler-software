/*
 * In- and output functions
 *
 */

void ioInit() {
  if (serialOn) Serial.println("IO init started");
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  pinMode(valveOpenPin, OUTPUT);
  digitalWrite(valveOpenPin, LOW);
  pinMode(valveClosePin, OUTPUT);
  digitalWrite(valveClosePin, LOW);
  pinMode(sdCardDetect, INPUT);
  digitalWrite(sdCardDetect, HIGH);
  pinMode(UVOffPin, OUTPUT);
  digitalWrite(UVOffPin, LOW);
  pinMode(powerOffPin, OUTPUT);
  digitalWrite(powerOffPin, LOW);
  if (serialOn) Serial.println("IO init done");
}

void closeValve() {
  valveState = VALVE_CLOSED;
  updateValveState(valveState);
  eepromWriteByte(addressValveState, valveState);
}

void openValve() {
  valveState = VALVE_OPEN;
  updateValveState(valveState);
  eepromWriteByte(addressValveState, valveState);
}

void updateValveState(byte openValve) {
  if(openValve) {
    logger.println("Opening valve!");
    digitalWrite(valveOpenPin, HIGH);
  } else {
    logger.println("Closing valve!");
    digitalWrite(valveClosePin, HIGH);
  }
  delay(tOn);
  digitalWrite(valveClosePin, LOW);
  digitalWrite(valveOpenPin, LOW);
}

void turnOffMCU() {
  logger.println("Powering off!");
  logger.flush();
  digitalWrite(powerOffPin, HIGH);
}

void deepSleep() {
  logger.println("Going into deep sleep!");
  digitalWrite(UVOffPin, HIGH);
  delay(1000);
  digitalWrite(UVOffPin, LOW);
  turnOffMCU();
}
//
