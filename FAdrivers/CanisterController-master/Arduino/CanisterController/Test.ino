/*
 * Test functions
 *
 */

void testAll() {
  if (!(eepromWriteDoubleTest() && eepromWriteByteTest() &&
        eepromWriteIntTest() && controlFileTest() && isSDCardPresentTest() &&
        eventLoggerTest())) {
    if (serialOn) Serial.println("ERROR: tests failed!");
  }
}

bool eepromWriteDoubleTest() {
  double in = 1434.222223452345345;
  byte address = 0x40;
  eepromWriteDouble(address, in);
  double out = eepromReadDouble(address);

  Serial.print("\nIn:  ");
  Serial.println(in, 50);
  Serial.print("\nOut:  ");
  Serial.println(out, 50);

  if (in != out) {
    Serial.println("ERROR: eepromWriteDoubleTest failed!");
    return false;
  } else {
    return true;
  }
}

bool eepromWriteByteTest() {
  byte in = 0x0A;
  byte address = 0x10;
  eepromWriteByte(address, in);
  byte out = eepromReadByte(address);

  Serial.print("\nIn: 0x");
  Serial.println(in, HEX);
  Serial.print("\nOut:  0x");
  Serial.println(out, HEX);

  if (in != out) {
    Serial.println("ERROR: eepromWriteByteTest failed!");
    return false;
  } else {
    return true;
  }
}

bool eepromWriteIntTest() {
  int in = 9992;
  byte address = 0x10;
  eepromWriteInt(address, in);
  int out = eepromReadInt(address);

  Serial.print("\nIn: ");
  Serial.println(in);
  Serial.print("\nOut: ");
  Serial.println(out);

  if (in != out) {
    Serial.println("ERROR: eepromWriteIntTest failed!");
    return false;
  } else {
    return true;
  }
}

bool isSDCardPresentTest() {
  if (isSDCardPresent()) {
    Serial.println("isSDCardPresent true!");
  } else {
    Serial.println("isSDCardPresent false!");
  }
  return true;
}

bool controlFileTest() {
  String fileName = "ctrl.txt";
  if (isFilePresentOnSDCard(fileName)) {
    Serial.println("ERROR: isFilePresentOnSDCard should be false!");
    return false;
  }

  createControlFile(fileName);

  if (!isFilePresentOnSDCard(fileName)) {
    Serial.println("ERROR: isFilePresentOnSDCard should be true!");
    return false;
  }

  deleteFile(fileName);

  if (isFilePresentOnSDCard(fileName)) {
    Serial.println(
        "ERROR: isFilePresentOnSDCard should be false (was deleted)!");
    return false;
  }

  return true;
}

bool eventLoggerTest() {
  if (serialOn) Serial.println("--- eventLoggerTest start() ---");
  readRTC();
  logger.println(timestampRTCString);
  logger.println("Test1");
  logger.println("stepPressureTarget calculated: " +
                 String(stepPressureTarget, 1));
  logger.println("string 1 + " + String("string 2"));

  logger.print("address: ");
  logger.print("0x");
  logger.print(addressSMSStatusByte, HEX);
  logger.print(", byte: 0x");
  logger.print(smsStatus, HEX);
  logger.println("");

  logger.print("Read double from EEPROM memory: ");
  logger.print(pressure24Hg, 5);
  logger.println("");

  if (serialOn) Serial.println("--- eventLoggerTest end() ---");

  return true;
}
