#include "EventLogger.h"

#include <SD.h>
extern byte ledPin;
extern byte serialOn;

EventLogger::EventLogger(int mode) {
  _mode = mode;
}

size_t EventLogger::write(uint8_t val) { return write(&val, 1); }

size_t EventLogger::write(const uint8_t *buf, size_t size) {
  if (_mode == SERIAL_MODE) {
    if (serialOn) {
      return Serial.write(buf, size);
    }
  } else {
    if(_arrIdx < MAX_NUMBER_OF_STRINGS) {
      _stringArray[_arrIdx] = new String((char *)buf);
      _arrIdx++;
    } else {
      Serial.println("ERROR: Can not handle more than " + String(_arrIdx) + " - ignoringe even!");
    }
    return size;  // Is this correct?
  }
}

void EventLogger::flush() {
  String fileName = "event.log";
  if (_mode == SD_CARD_MODE) {
    if (serialOn) Serial.println("Flushing " + String(_arrIdx) + " events to file: " + fileName);

    File dataFile = SD.open(fileName, FILE_WRITE);
    // if the file is available, write to it:
    if (dataFile) {
       if (serialOn) Serial.println("Was able to open " + fileName);
       dataFile.println("");
       for (int i = 0; i < _arrIdx; i++) {
         dataFile.print(*_stringArray[i]);
      }
      dataFile.close();
      if (serialOn) Serial.println("Flushing events DONE!");
    } else {
      if (serialOn) Serial.println("Flushing FAILED - could not open " + fileName);
      if (serialOn) Serial.println("Events: ");
      for (int i = 0; i < _arrIdx; i++) {
         if (serialOn) Serial.print(*_stringArray[i]);
      }
    }

    for (int i = 0; i < _arrIdx; i++) {
      delete _stringArray[i];
    }
    _arrIdx = 0;
  }
}
