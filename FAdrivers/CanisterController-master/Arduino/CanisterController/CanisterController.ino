/* Canister timer and controller
 * Rev. 01: Only datalogging test
 * Rev. 02: Complete functionality - See details in "Notes" tab
 * Rev. 03: Implemented version 7 of the flow chart.
 * Hardware used: See details in "Notes" tab
 */

#include <Wire.h>
#include "uRTCLib.h"
uRTCLib rtc;
#include <ZEeprom.h>
#define EEPROM_ADDRESS  0x50 // I2C address
ZEeprom * eeprom;
#include <SPI.h>
#include <SD.h>
#include <MKRGSM.h> // Include the GSM library
#include "EventLogger.h"
EventLogger logger(SD_CARD_MODE); // (SERIAL_MODE);

// initialize the library instance
GSM gsmAccess;
GSM_SMS sms;

// Debug setting
const byte debug = 1; // 1 = debugging, 0 = not

// SD card chip select
//const byte chipSelect = SDCARD_SS_PIN;
const byte sdChipSelect = 7;

// Pins
const byte ledPin =         3; // On board LED
const byte valveOpenPin =   4;
const byte valveClosePin =  5;
const byte sdCardDetect =   6; // SD card detect
const byte pressureIn =    A1; // Analog voltage from pressure sensor
const byte UVOffPin =      A3; // The UVPO pin
const byte timerOutPin =   A4; // Output from timer
const byte powerOffPin =   A5; // Go to sleep (timer)
const byte psuVoltagePin = A6; // Battery voltage

// Constants
const float batteryMinimumStartVoltage = 20.0; // Start up minimum voltage
const float minimumBatteryVoltage =      16.0; // Session minimum voltage
const long baudRate =                   38400; // Baud rate
const long delayTime =                  60000; // Delay time
const unsigned long nAvg =               1000; // Number of samples to average
const unsigned int tOn =                   75; // Excitation time to valve solenoid in ms
const unsigned int elapsedHoursTarget =   336; // 14 days in hours (14 days x 24 hours)

// GSM
const char PINNUMBER[] =         "3250"; // PIN number for the SIM card
const char remoteNum[] = "004526591373"; // Telephone number to send sms
const char txtMsg[] =        "Mother: "; // Message

// ADC
const double press_gain = 0.4166666667;
const double press_offset =      -0.25;
const float timer_gain =        2.006;
const float battery_gain =      7.375;
byte valveState =                   0; // Valve state: 1 = open, 0 = closed
byte smsStatus =                    0;

// EEPROM
// Bytes
const byte addressSMSStatusByte =      0x0;
const byte addressValveState =         0x1; // 1 = open, 0 = closed
const byte addressPowerUpCounter =    0x09;
const byte addressSecOld =            0x0A;
const byte addressMinOld =            0x0B;
const byte addressHourOld =           0x0C;
const byte addressDayOld =            0x0D;
const byte addressMonthOld =          0x0E;
const byte addressYearOld =           0x0F;
// Integers
const byte addressElapsedHours =       0x10; // Elapsed hours
const byte addressControllerSerial =   0x1E; // Uniq serial number
// Double
const byte addressStartPressure = 0x40;

// Variables
byte serialOn =           0;
byte eepromDetailedLog =  0;
float timerVoltage =    0.0; // Voltage from timer
float batteryVoltage =  0.0; // Power supply voltage
byte powerUpCounter =     0; // How many times have the MCU been started up
int controllerSerial =    0;
unsigned int elapsedHours =                    0;
// Pressure
double pressureVoltage =                     0.0; // Pressure voltage
double pressure =                            0.0; // Pressure [bar]
double deltaStepPressure =                   0.0; // Step Pressure (delta) [bar]
double stepPressureTarget =                  0.0; // Absolute target Step Pressure [bar]
double startPressure =                       0.0; // Pressure [bar]
// Pressures constants (double)
const double pressure2Hg =        0.932272236842; // 2 inHg in bar
const double pressure10Hg =       0.661361184211; // 19 inHg in bar
const double pressure24Hg =       0.187266842105; // 24 inHg in bar

unsigned long i =         0; // Average

// RTC
float rtcTemp =              0.0;
byte sec =                    0;
byte min =                    0;
byte hour =                   0;
byte day =                    0;
byte month =                  0;
byte year =                   0;
byte secOld =                 0;
byte minOld =                 0;
byte hourOld =                0;
byte dayOld =                 0;
byte monthOld =               0;
byte yearOld =                0;

// SD variables
String dataString =           "";
char timestampRTCString[50];
String controlFileName =      "control.txt";
String dataFileName =         "datalog.txt";

// GSM
bool connected =           false;
const byte SMS_START = 1;
const byte _10HG_WARNING = 2;
const byte _2HG_END_OF_SESSION = 3;
const byte SD_CARD_PROBLEM = 4;
const byte PRESSURE_SESSION_ABORT = 5;
const byte VOLTAGE_SESSION_ABORT = 6;
const byte END_OF_SESSION = 10;

// Valve state
const byte VALVE_OPEN =   1;
const byte VALVE_CLOSED = 0;

void setup() {
  setupInit(); // In "Control" tab
}

void loop() {
control();
delay(delayTime);

/*  // For testing
  eepromDetailedLog = 1;
  testAll();
  logger.flush();
  delay(5000);*/
}
