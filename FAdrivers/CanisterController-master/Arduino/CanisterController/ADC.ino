/*
 * ADC
 *
 * ToDo:
 * Very high resolution on pressure ADC
 *
 */

void adcInit() {
  analogReadResolution(12); // Lifting the resolution from 10 to 12 bit
  logger.println("ADC init done, now 12-bit");
}

void readTimerVoltage() {
  if (serialOn) Serial.println("Reading Timer Voltage");
  timerVoltage = (float(analogRead(timerOutPin))/4096.0*3.3)*timer_gain;
  logger.print("Timer Voltage: ");
  logger.print(timerVoltage, 3);
  logger.println("");
}

void readBatteryVoltage() {
  if (serialOn) Serial.println("Reading Battery Voltage");
  batteryVoltage = (float(analogRead(psuVoltagePin))/4096.0*3.3)*battery_gain;
  logger.print("Battery Voltage: ");
  logger.print(batteryVoltage, 3);
  logger.println("");
}

//
