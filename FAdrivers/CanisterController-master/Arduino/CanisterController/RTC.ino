/*
 * RTC functions
 *
 */
void updateISO8601TimeString() {
  // ISO 8601: 2020-08-12T17:29:07Z
  // HET String: y-m-d h-m-s
  sprintf( timestampRTCString, "20%02d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, min, sec );
}

//##### Read the RTC
void readRTC() {
  if (serialOn) Serial.println("RTC update started");
  rtc.refresh(); // Update from RTC chip
  rtcTemp = float(rtc.temp())/100.0;
  sec = rtc.second();
  min = rtc.minute();
  hour = rtc.hour();
  day = rtc.day();
  month = rtc.month();
  year = rtc.year();

  updateISO8601TimeString();

  logger.print("RTC updated: ");
  logger.println(timestampRTCString);
}

// RTC init
void rtcInit() {
  if (serialOn) Serial.println("RTC init");
  rtc.set_rtc_address(0x68);
  rtc.set_model(URTCLIB_MODEL_DS3232);
  rtc.alarmClearFlag(URTCLIB_ALARM_1); // Clear both alarms
  rtc.alarmClearFlag(URTCLIB_ALARM_2);
  if (serialOn) Serial.println("RTC init done");
}
