/*
 * GSM stuff
 *
 * Only use GSM features w. closed valve
 *
 */

void gsmStartingUp() {
}

void gsmWarning() {
}

void gsmShutDown() {
}

void gsmLowPower() {
}

void gsmSendSms(byte status) {
  String smsText = "";
  eepromWriteByte(addressSMSStatusByte, status);
  switch (status) {
    case SMS_START:
      smsText = "Starting measurement";
      break;
    case _10HG_WARNING:
      smsText = "10 inHg warning";
      break;
    case _2HG_END_OF_SESSION:
      smsText = "2 inHg, end of session";
      break;
    case SD_CARD_PROBLEM:
      smsText = "SD card problem";
      break;
    case PRESSURE_SESSION_ABORT:
      smsText = "Pressure session abort";
      break;
    case VOLTAGE_SESSION_ABORT:
      smsText = "Voltage session abort";
      break;
    case END_OF_SESSION:
      smsText = "end of session shutdown!";
      break;
    default:
      // statements
      logger.println("ERROR - unsupported sms status: " + status);
      return;
  }
  logger.println("TODO: send sms string (10 attempts, Date, time, start pressure, step pressure, bat voltage): " + smsText);
}

/*
  while (!connected) {
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      connected = true;
    } else {
      if(serialOn) Serial.println("Not connected");
      delay(1000);
    }
  }
  if(serialOn) Serial.println("GSM initialized");



void loop() {
  // sms text
  if(serialOn) {
    Serial.print("Mobile number: ");
    Serial.println(remoteNum);
    Serial.print("Sending message: ");
    Serial.println(txtMsg);
  }
  // send the message
  sms.beginSMS(remoteNum);
  sms.print(txtMsg);
  sms.endSMS();
  if(serialOn) Serial.println("COMPLETE!");
  digitalWrite(LED_BUILTIN, LOW);
  while(1);
}

*/

// THE END
