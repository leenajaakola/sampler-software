/*


*/

void readPressure() {
  pressure = 0.0;
//  delay(100); // Maybe we need a little while to get steady pressure measuring
  for (i = 0; i < nAvg; i++) {
    pressureVoltage = double(analogRead(pressureIn)) / 4096.0 * 3.3;
    pressure += ((pressureVoltage * press_gain) + press_offset) * 1000.0; // mbar
//    pressure += (pressureVoltage * press_gain) + press_offset;
  }
  pressure /= double(nAvg);//*/
  pressure /= 1000.0; // mbar to bar
  logger.print("Pressure Voltage: ");
  logger.print(pressureVoltage, 3);
  logger.println("\t");
  logger.print("Pressure: ");
  logger.print(pressure, 4);
  logger.println(" bar");
}


void calculateStepPressureTarget() {
  logger.println("calculatePressureTarget()");
  deltaStepPressure = (pressure2Hg - startPressure) / double(elapsedHoursTarget);
  stepPressureTarget = (deltaStepPressure * double(elapsedHours+1)) + startPressure;
  logger.print("stepPressureTarget calculated: ");
  logger.println(stepPressureTarget, 5);
}
//
