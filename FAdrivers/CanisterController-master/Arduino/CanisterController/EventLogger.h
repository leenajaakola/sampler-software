/*
 * EventLogger.h - Class for logging event to a log file on a SD card or to the serial monitor.
 * TODO: Make a better version from https://playground.arduino.cc/Code/Printclass/ and LiquidCrystal.h
*/
#ifndef EventLogger_h
#define EventLogger_h

#include "Arduino.h"

const int SERIAL_MODE = 0;
const int SD_CARD_MODE = 1;
const int MAX_NUMBER_OF_STRINGS = 500;

class EventLogger : public Print
{
  public:
    EventLogger(int mode);
    virtual size_t write(uint8_t val);
    virtual size_t write(const uint8_t *buf, size_t size);
    virtual void flush();
  private:
    int _mode;
    String *_stringArray[MAX_NUMBER_OF_STRINGS];
    int _arrIdx = 0;
};

#endif
