/*
 * Krav:
 * 14 dage på 2Ah Dewalt batteri.
 * 
 * Igangsætning:
 * Mål starttryk og beregn trykhop for hver time.
 * Send SMS til hardkodet mobil nr om, at den starter og hvad starttryk er.
 * 
 * Ved hvert "wake up" (1 min):
 * Se om der skal åbnes for ventil (er der gået en time siden sidste gang der blev åbnet).
 * Tjek i Eeprom hvor langt vi er kommet (antal timer).
 * Beregn hvilket tryk den må stige til for den pågældende time.
 * Åben ventil indtil nyt threshold er nået.
 * Luk og kør lukket resten af den time.
 * Lav trykmåling hvert min. (Husk energiberegning).
 * 
 * Advarsler og sluk:
 * Ved 10"Hg (339mbar vacuum = 1.0 - 0.339 = 0.661bar) : Send advarsel.
 * Ved 2"Hg (67.7mbar vacuum = 1.0 - 0.0677 = 0.932bar): Luk ventil, send SMS og aktiver "luk ned" procedure.
 * 
 * Hardware descrition:
 * Arduino MKR GSM 1400 or MKR Zero
 * micro SD card (SPI bus). CS pin: 7.
 * RTC (DS3231) (I2C address: 0x68)
 * Low power timer for extended battery life. When power is applied the low power
 * timer output is ON. This gives power for the +3V3 system and the firmware control 
 * when the system goes to sleep (timer running) by pulling the MCU turn off pin.
 * When the timer is running for the preset time the +3V3 is off. When the
 * preset time is elapsed the timer output goes high and +3V3 is applied.
 * There is at UVPO (Under Voltage Power Off) circuit that can be triggered in firmware.
 * The circuit is reset when power is applied or power is turned on. In firmware the
 * battery voltage is measured at every processor run and if the battery voltage is
 * measured too low the UVPO is triggered and the timer can not wake up again. The OVPO
 * must be high for at least half a second to be activated.
 * 
 * Inputs:
 * micro SD Card Detect pin: 6.
 * Pressure transmitter pin: A1.
 * Timer output pin: A4.
 * Battery voltage pin: A6.
 * Outputs:
 * LED pin: D3.
 * Valve 1 pin: D4.
 * Valve 2 pin: D5.
 * micro SD Chip Select pin: D7.
 * Under Voltage Power Off pin: A3.
 * MCU turn off pin: A5.
 */


/*  Old thoughts...
 *  Concerns:
 *  Manual valve is closed - user forgot to open
 *  Manual valve is closed - user closed it in session
 *  How to stop a session if wanted? - Google sheets data -> final shut down precedure
 *  Final pressure is too close to atmospheric pressure
 *  Expect high battery voltage (fully charged up) during start up? - 20V?
 *
 *  Remember list:
 *  RTC not running (maybe missing power) - must not stop session
 *  Battery level during startup - 20V? - done
 *  Pressure warning - 10" Hg - remember that the message is sent (EEPROM)
 *  GSM timeout
 *  Pressure final target reached
 *  Pressure calibration
 *
 *  Wake up precedure
 *  Power up
 *  Init needed stuff...
 *  Read pressure
 *  Measure battery voltage - Voltage high enough?
 *    Yes - Battery ok && Vacuum pressure higher than Stop pressure (2 inHg)
 *      Read RTC
 *      Write EEPROM - RTC data
 *      Read EEPROM - Session flag set?
 *      Yes - In a session
 *        Read EEPROM - sms status
 *        Pressure: warning pressure (10 inHg) reached && !(10" Hg message sent)?
 *        yes - Vacuum pressure below warning level and no message is sent
 *          Sent SMS "10 inHg level vacuum reached"
 *          Write EEPROM - 10" Hg pressure message sent
 *        Read EEPROM - Valve state
 *        Valve open?
 *        Yes - valve is open
 *          Read EEPROM - Pressure target to close valve
 *          Pressure: Target reached?
 *            No - More pressure needed
 *              Save data to SD if SD Card is present
 *              Go to sleep
 *            Yes - Pressure target reached
 *              Close valve
 *              Save data to SD if SD Card is present
 *              Go to sleep
 *        No - valve is closed
 *          Time to open valve?
 *          Yes - Time to open valve
 *            Write EEPROM - Session Hour + 1
 *            Open valve
 *          
 *          No - Keep valve closed for now
 *          Save data to SD if SD Card is present
 *          Go to sleep
 *      No - Starting up a session
 *        Battery voltage > Start voltage (20V ?)
 *          Yes - Battery voltage indicate fully charged battery
 *            Pressure below threshold for "ok" starting pressure?
 *              Yes - Start pressure ok (>24" inHg vacuum)
 *                Write EEPROM - Set session flag to "In session"
 *                Calculate step pressure
 *                Write EEPROM - step pressure
 *                Write EEPROM - Session Hour = 0
 *                Send init SMS: Date, time, start pressure, step pressure and battery voltage
 *                Open valve
 *                Save data to SD if SD Card is present
 *                Go to sleep
 *              No - Start vacuum too low - Deny a session
 *                Send SMS: Start pressure too high
 *                Go to sleep
 *          No - Battery is not fully charged
 *            Close valve
 *            Send SMS: Start battery voltage too low - Battery not fully charged
 *            Pull the UVLO pin high
 *            Wait 1 sec. to overcome UVLO HW delay filter
 *            Put circuit to sleep - Never waking up again
 *  No - Battery voltage too low - Shut down sequence
 *    Close valve
 *    Save data to SD if SD Card is present
 *    Send last SMS regarding low battery and final pressure measuring
 *    Write EEPROM - Reset warning pressure message sent
 *    Write EEPROM - Reset session flag
 *    Pull the UVLO pin high
 *    Wait 1 sec. to overcome UVLO HW delay filter
 *    Put circuit to sleep - Never waking up again
 *
 *
 */

//
