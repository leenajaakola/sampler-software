/*
 * DMR Canister Control loop
 *
 * The numbers in [] refer to the equivalent step in the flow chart.
 */

// [1]
void setupInit() {
  ioInit();
  serialInit();
  adcInit();
  i2cInit();  // In "Serial" tab
  rtcInit();
  eepromInit();
  SD_init();
}

void control() {
  logger.println("Starting control loop!");

  // [2]
  logger.println("[2]");
  readBatteryVoltage();

  // [3]
  logger.println("[3]");
  readRTC();

  // [10]
  logger.println("[10]");
  if (isSDCardPresent()) {
    // [40]
    logger.println("[40]");

    if (isFilePresentOnSDCard(controlFileName)) {
      logger.println(controlFileName + " is present");

      // [110]
      logger.println("[110]");
      if (batteryVoltage > minimumBatteryVoltage) {

        // [100]
        logger.println("[100]");
        smsStatus = eepromReadByte(addressSMSStatusByte);
        valveState = eepromReadByte(addressValveState);
        powerUpCounter = eepromReadByte(addressPowerUpCounter);
        elapsedHours = eepromReadInt(addressElapsedHours);
        controllerSerial = eepromReadInt(addressControllerSerial);
        startPressure = eepromReadDouble(addressStartPressure);
        // eepromReadOldRTC(); // TODO

        logger.println("[101]");
        logger.print("powerUpCounter (EEPROM): ");
        logger.println(powerUpCounter);
        powerUpCounter++;  // [101]
        logger.print("powerUpCounter (updated): ");
        logger.println(powerUpCounter);

        // [180]
        logger.println("[180]");
        if (powerUpCounter > 59) {  // Time to open valve
          logger.println("[181]");
          powerUpCounter = 0;       // [181]
          logger.println("powerUpCounter = 0");
          logger.println("[303]");
          elapsedHours++;
          logger.print("elapsedHours = ");
          logger.println(elapsedHours);
          logger.println("[304]");
          eepromWriteInt(addressElapsedHours, elapsedHours);  // [103]
          // hour++; // [183] TODO!
          // eepromWriteRTCAsOld(); // [183]
        }

        logger.println("[103]");
        eepromWriteInt(addressPowerUpCounter, powerUpCounter);  // [102]

        logger.println("[111]");
        readPressure();  // [111]

        logger.println("[184]");
        calculateStepPressureTarget();  // [184]
        logger.print("stepPressureTarget = ");
        logger.println(stepPressureTarget, 5);

        // [140]
        logger.println("[140]");
        if (pressure > pressure10Hg) {
          // [150]
          logger.println("[150]");
          if (smsStatus != _10HG_WARNING) {
            logger.println("[151]");
            gsmSendSms(_10HG_WARNING);  // [151]
          }
        }

        // [170]
        logger.println("[170]");
        if (valveState == VALVE_OPEN) {
          // [210]
          logger.println("[210]");
          if (pressure >= pressure2Hg) {
            logger.println("[220]");
            closeValve();  // [220]
            logger.println("[221]");
            saveToSD();  // [221]
            logger.println("[222]");
            gsmSendSms(END_OF_SESSION);  // [222]
            logger.println("[223]");
            deepSleep();  // [223] TODO: Write Eventlog to sd before closing the
                          // shop
            return;
          } else {
//            logger.println("[211]");
//            calculateStepPressureTarget();  // [211]
            logger.println("[230]");
            if (pressure > stepPressureTarget) {
              logger.println("[231]");
              closeValve();  // [231]
              logger.println("[232]");
              saveToSD();  // [232]
              logger.println("[233]");
              turnOffMCU();  // [233]
              return;
            } else {
              logger.println("[240]");
              saveToSD();  // [240]
              logger.println("[241]");
              turnOffMCU();  // [241]
              return;
            }
          }
        } else {
          // [280]
          logger.println("[280]");
          if (powerUpCounter == 0) {  // Time to open valve
            // [190]
            logger.println("[190]");
            if (pressure < stepPressureTarget) {
              logger.println("[191]");
              openValve();  // [191]
              logger.println("[192]");
              saveToSD();  // [192]
              logger.println("[193]");
              turnOffMCU();  // [193]
              return;
            } else {
              logger.println("[200]");
              saveToSD();  // [200]
              logger.println("[201]");
              turnOffMCU();  // [201]
              return;
            }
          } else {
            logger.println("[200]");
            saveToSD();  // [200]
            logger.println("[201]");
            turnOffMCU();  // [201]
            return;
          }
        }
      } else {
        logger.println("[120]");
        // [120]
        if (valveState == VALVE_OPEN) {
          logger.println("[130]");
          closeValve();  // [130]
        }
        logger.println("[121]");
        gsmSendSms(VOLTAGE_SESSION_ABORT);  // [121]
        logger.println("[122]");
        deepSleep();  // [122] TODO: Write Eventlog to sd before closing the
                      // shop
        return;
      }
    } else {
      logger.println(controlFileName + " is not present => Starting a new session!");
      // [50]
      logger.println("[50]");
      if (batteryVoltage > batteryMinimumStartVoltage) {
        logger.println("[70]");
        // [70]
        if (pressure < pressure24Hg) {
          logger.println("[71]");
          createControlFile(controlFileName);  // [71]
          logger.println("[72]");
          readPressure();  // [72]
          startPressure = pressure;
          logger.println("[73]");
          eepromWriteDouble(addressStartPressure, startPressure);  // [73]
          logger.println("[74]");
          powerUpCounter = 0;  // [74]
          logger.println("[75]");
          eepromWriteInt(addressPowerUpCounter, powerUpCounter);  // [75]
          logger.println("[76]");
          elapsedHours = 0;                                   // [76]
          eepromWriteInt(addressElapsedHours, elapsedHours);  // [76]
          // eepromWriteRTCAsOld(); // [77] TODO
          logger.println("[78]");
          openValve();  // [78]
          logger.println("[79]");
          saveToSD();  // [79]
          logger.println("[80]");
          gsmSendSms(SMS_START);  // [80]
          logger.println("[81]");
          turnOffMCU();  // [81]
          return;
        } else {
          logger.println("[90]");
          gsmSendSms(PRESSURE_SESSION_ABORT);  // [90]
          logger.println("[91]");
          deepSleep();  // [91] TODO: Write Eventlog to sd before closing the
                        // shop
          return;
        }
      } else {
        logger.println("[60]");
        gsmSendSms(VOLTAGE_SESSION_ABORT);  // [60]
        logger.println("[61]");
        deepSleep();  // [61] TODO: Write Eventlog to sd before closing the shop
        return;
      }
    }

  } else {
    valveState = eepromReadByte(addressValveState);
    // [20]
    logger.println("[20]");
    if (valveState == VALVE_OPEN) {
      logger.println("[30]");
      // [30] close valve
      closeValve();
    }
    logger.println("[21]");
    gsmSendSms(SD_CARD_PROBLEM);  // [21]
    logger.println("[22]");
    deepSleep();  // [22] TODO: Write Eventlog to sd before closing the shop
    return;
  }
}

//
