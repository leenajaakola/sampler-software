/*
 * EEPROM stuff
 *
 */

void eepromInit() {
  eeprom = new ZEeprom();
  eeprom->begin(Wire,AT24Cxx_BASE_ADDR,AT24C02);
  if(serialOn) Serial.println("EEPROM Init done");
}

byte eepromReadByte(byte address) {
  byte data = eeprom->readByte(address);
  if(serialOn && eepromDetailedLog) {
    Serial.print("Read byte from EEPROM memory address: 0x"); // Read a byte at address in EEPROM memory.
    Serial.print(address, HEX);
    Serial.print(", byte: 0x");
    Serial.println(data, HEX);
//    Serial.println(")");
  }
  return data;
}

void eepromReadOldRTC() {
  secOld = eepromReadByte(addressSecOld);
  minOld = eepromReadByte(addressMinOld);
  hourOld = eepromReadByte(addressHourOld);
  dayOld = eepromReadByte(addressDayOld);
  monthOld = eepromReadByte(addressMonthOld);
  yearOld = eepromReadByte(addressYearOld);

  if(serialOn && eepromDetailedLog) {
    Serial.print("Read old RTC from eeprom Y, M, D  H, M, S"); // Read a byte at address in EEPROM memory.
    Serial.print(yearOld);
    Serial.print(", ");
    Serial.print(monthOld);
    Serial.print(", ");
    Serial.print(dayOld);
    Serial.print("\t");
    Serial.print(hourOld);
    Serial.print(", ");
    Serial.print(minOld);
    Serial.print(", ");
    Serial.println(secOld);
//    Serial.println(")");
  }
}

void eepromWriteRTCAsOld() {
  if(serialOn && eepromDetailedLog) Serial.println("write RTC to oldRTC data in eeprom");
  eepromWriteByte(addressSecOld, sec);
  eepromWriteByte(addressMinOld, min);
  eepromWriteByte(addressHourOld, hour);
  eepromWriteByte(addressDayOld, day);
  eepromWriteByte(addressMonthOld, month);
  eepromWriteByte(addressYearOld, year);

    if(serialOn && eepromDetailedLog) {
    Serial.print("Wrote old RTC bytes to eeprom "); // Read a byte at address in EEPROM memory.
    Serial.print(year);
    Serial.print(", ");
    Serial.print(month);
    Serial.print(", ");
    Serial.print(day);
    Serial.print(", ");
    Serial.print(hour);
    Serial.print(", ");
    Serial.print(min);
    Serial.print(", ");
    Serial.println(sec);
//    Serial.println(")");
    }
}

void eepromWriteByte(byte address, byte byteToWrite) {
  eeprom->writeByte(address, byteToWrite); // Write a byte at address in EEPROM memory.
  delay(10); // Write cycle time (tWR). See EEPROM memory datasheet for more details.
  if(serialOn && eepromDetailedLog) {
    Serial.print("Wrote byte to EEPROM memory (address: 0x"); // Read a byte at address in EEPROM memory.
    Serial.print(address, HEX);
    Serial.print(", byte: 0x");
    Serial.print(byteToWrite, HEX);
//    Serial.println(")");
  }
}

double eepromReadDouble(byte address) {
  double doubleOut;
  for (int i = 0; i < sizeof(double); i++) {
    ((byte *) & doubleOut)[i] = eepromReadByte(address+i);
  }
  if(serialOn && eepromDetailedLog) {
    Serial.print("Read double from EEPROM memory aadress: ");
    Serial.print(address, HEX);
    Serial.print(" Double: ");
    Serial.println(doubleOut, 5);
//    Serial.println("");
  }
  return doubleOut;
}

void eepromWriteDouble(byte address, double doubleToWrite) {
  byte byteToWrite;
  for (int i = 0; i < sizeof(double); i++) {
    byteToWrite = ((byte *)&doubleToWrite)[i];
    eepromWriteByte(address+i, byteToWrite);
  }
  if(serialOn && eepromDetailedLog) {
    Serial.print("Wrote double to EEPROM memory address: ");
    Serial.print(address, HEX);
    Serial.print(" Double: ");
    Serial.println(doubleToWrite, 5);
//    Serial.println("");
  }
}

int eepromReadInt(byte address) {
  int intOut;
  for (int i = 0; i < sizeof(int); i++) {
    ((byte *) & intOut)[i] = eepromReadByte(address+i);
  }
  if(serialOn && eepromDetailedLog) {
    Serial.print("Read int from EEPROM memory address: ");
    Serial.print(address, HEX);
    Serial.print(" Int: ");
    Serial.println(intOut);
//    Serial.println("");
  }
  return intOut;
}

void eepromWriteInt(byte address, int intToWrite) {
  byte byteToWrite;
  for (int i = 0; i < sizeof(int); i++) {
    byteToWrite = ((byte *)&intToWrite)[i];
    eepromWriteByte(address+i, byteToWrite);
  }

  if(serialOn && eepromDetailedLog) {
    Serial.print("Wrote int to EEPROM memory address: ");
    Serial.print(address, HEX);
    Serial.print(" Int: ");
    Serial.println(intToWrite);
//    Serial.println("");
  }
}


//
