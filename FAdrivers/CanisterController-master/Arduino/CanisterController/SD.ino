/*
 * SD card functions
 * Open, write and close can tage around 40ms
 * SD card attached to SPI bus as follows:
 * CS -   pin 7
 * MOSI - pin 8
 * CLK -  pin 9
 * MISO - pin 10
 *
 * Data to be logged:
 * -----------------
 * Date
 * Time of day
 * elapsedHours
 * Pressure [mbar]
 * Valve State [0/1]
 * Step Pressure Target [mbar]
 * PSU voltage [V]
 * RTC internal chip temperature [deg.C]
 */

// SD card inserted?
bool isSDCardPresent() {
  if(serialOn) Serial.print("SD card inserted: ");
  if (!digitalRead(sdCardDetect)) { // SD card inserted?
    if(serialOn) Serial.println("True");
    return true;
  } else {
    if(serialOn) Serial.println("False");
    return false;
  }
}

// SD card routine
void saveToSD() {
  digitalWrite(ledPin, HIGH);
  if (isSDCardPresent()) {
    if(serialOn) Serial.println("\nMaking datapoint");
    digitalWrite(ledPin, HIGH);
    SD_init();
    if(!sdFileExists()) {
      writeHeaderToFile();
    }
    writeDataToFile();
    digitalWrite(ledPin, LOW);
    if(serialOn) Serial.println("Datapoint done");
  } else {
    if(serialOn) Serial.println("ERROR: Could not detect SD Card!");
    // TODO: eepromWrite(); smsStatus = 4
    // SMS: SD card problem
  }
  digitalWrite(ledPin, LOW);
}

//##### SD Card Header
void writeHeaderToFile() {
  if(serialOn) Serial.print("Generating SD header... \t");
  // make a string for assembling the header to log:
  dataString = "";
  dataString += ("\r\nDMR Pressure Logger\r\n\r\n");
  dataString += ("Start time: ");
  dataString += timestampRTCString;
  dataString += ("\r\n");
  printToSD();
  dataString = "";
//  dataString += ("\r\nTimestamp,Elapsed hours [h],Pressure [mBar],ValveState [0/1],Step Pressure Target [mBar],Internal temp. [gr.C],PSU voltage [V]"); // The original one
  dataString += ("\r\nTimestamp,Elapsed hours [h],powerUpCounter [n],Pressure [mBar],ValveState [0/1],Step Pressure Target [mBar],Internal temp. [gr.C],PSU voltage [V]"); // TODO: For testing only
  printToSD();
  if(serialOn) Serial.println("SD header done");
}

void writeDataToFile() {
  if(serialOn) Serial.println("Data to SD started");
  dataString = "";
  dataString += timestampRTCString;
  dataString += ",";
  dataString += String(elapsedHours); dataString += ",";
  dataString += String(powerUpCounter); dataString += ","; // TODO: For testing only
  dataString += String(pressure*1000.0, 1); dataString += ",";
  dataString += String(valveState); dataString += ",";
  dataString += String(stepPressureTarget*1000.0, 1); dataString += ",";
  dataString += String(rtcTemp, 2); dataString += ",";
  dataString += String(batteryVoltage, 3);
  if(serialOn) Serial.println(dataString);
  printToSD();
  if(serialOn) Serial.println("Data to SD ended");
}

void printSDEnd() {
  dataString = "";
  dataString += "\r\nEnd of data...\r\n\r\n\r\n";
  printToSD();
}

void printToSD() {
// open the file. note that only one file can be open at a time,
// so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
// if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
  } else {
    // if the file isn't open, pop up an error:
    if (serialOn) Serial.println("Error opening datalog.txt");
  }
  dataString = "";
}

void SD_init() {
  if (!SD.begin(sdChipSelect)) {
    if (serialOn) Serial.println("SD card failed, or not present");
  } else {
    if (serialOn) Serial.println("SD card initialized.");
  }
}

// SD card file look up - is file present
byte sdFileExists() {
  byte result = 0;
  if (SD.exists("datalog.txt")) {
    if(serialOn) Serial.println("datalog.txt exists");
    result = 1; // File exists
  } else {
    if(serialOn) Serial.println("datalog.txt doesn't exist");
    result = 0; // File doesn't exist
  }
  return result;
}

// is control file present
bool isFilePresentOnSDCard(const String& fileName) {
  byte result = 0;
  if (SD.exists(fileName)) {
    if(serialOn) Serial.println(fileName + " exists");
    return true; // File exists
  } else {
    if(serialOn) Serial.println(fileName + " does not exist");
    return false;
  }
}

void createControlFile(const String& fileName) {
  digitalWrite(ledPin, HIGH);
  File dataFile = SD.open(fileName, FILE_WRITE);
// if the file is available, write to it:
  if (dataFile) {
    dataFile.println("Start");
    dataFile.close();
  } else {
    // if the file isn't open, pop up an error:
    if (serialOn) Serial.println("could not open " + fileName);
    // TODO SEND SMS
  }
  digitalWrite(ledPin, LOW);
}

void deleteFile(const String& fileName) {
  SD.remove(fileName);
}

//
