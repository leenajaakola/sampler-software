/* 
 * Board: MKR Zero
 * Systemet kører på 3,3 Volt
 */

// Pins
const byte UVOffPin =            A3;
const byte powerOffPin =         A5;
const byte ledPin =               3;
const byte valve1Pin =            4;
const byte valve2Pin =            5;
//const byte ledOnBoard = LED_BUILTIN;

// Constants
const unsigned int bRate = 38400;
const unsigned int dTime =  2000;
const unsigned int tOn =      75;

// Variables
byte serialOn =  0;
byte vState =    0; // Valve state

void setup() {
  pinMode(UVOffPin, OUTPUT);
  digitalWrite(UVOffPin, LOW);
  pinMode(powerOffPin, OUTPUT);
  digitalWrite(powerOffPin, LOW);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  pinMode(valve1Pin, OUTPUT);
  digitalWrite(valve1Pin, LOW);
  pinMode(valve2Pin, OUTPUT);
  digitalWrite(valve2Pin, LOW);
//  pinMode(ledOnBoard, OUTPUT);
//  digitalWrite(ledOnBoard, LOW);
  Serial.begin(bRate);
  while(!Serial) {
    if(millis() >= 5000) {
      break;
    }
  }
  if(Serial) serialOn = 1;
  else serialOn = 0;
  if(serialOn) Serial.println("\nSerial online...\n");
  digitalWrite(ledPin, HIGH);
}

void loop() {
  if(vState) {
    vState = 0;
    digitalWrite(valve2Pin, HIGH);
    if(serialOn) Serial.println("Valve OFF");
    delay(tOn);
    digitalWrite(valve2Pin, LOW);
  }
  else {
    vState = 1;
    digitalWrite(valve1Pin, HIGH);
    if(serialOn) Serial.println("Valve ON");
    delay(tOn);
    digitalWrite(valve1Pin, LOW);
  }
  delay(dTime);
}
