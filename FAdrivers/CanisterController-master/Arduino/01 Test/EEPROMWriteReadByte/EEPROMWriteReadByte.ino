/**************************************************************************/
/**
 * \brief EEPROM 24C01 /02/04/08/16/32/64/128/254/512/1024 library for Arduino - Demonstration program
 */

#include <Wire.h>

#include <ZEeprom.h>
#define EEPROM_ADDRESS  0x50 // I2C address
ZEeprom * eeprom;

void setup() {
  Serial.begin(38400);
  delay(3000);
  eeprom= new ZEeprom();
  eeprom->begin(Wire,AT24Cxx_BASE_ADDR,AT24C02);
  const byte address = 0;
  Serial.println("\n\nWrite byte to EEPROM memory...");
  eeprom->writeByte(address, 0xBB); // Write a byte at address 0 in EEPROM memory.
  delay(10); // Write cycle time (tWR). See EEPROM memory datasheet for more details.
  Serial.println("Read byte from EEPROM memory..."); // Read a byte at address 0 in EEPROM memory.
  byte data = eeprom->readByte(address);
  Serial.print("Read byte = 0x");
  Serial.print(data, HEX);
  Serial.println("");
}


void loop() {
}
