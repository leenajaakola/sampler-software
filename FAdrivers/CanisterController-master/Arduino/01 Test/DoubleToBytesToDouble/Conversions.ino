/*
 * 
 */

// Float or double -> bytes
void loop1() {
  Serial.println("Converting float or double to array of bytes...");
  for (size_t i = 0; i < sizeof valueIn; i++) {
    ArrayOfBytes[SpecificLocation + i] = ((byte *)&valueIn)[i];
    Serial.print(((byte *)&valueIn)[i], HEX);
    Serial.print("\t");
  }
  Serial.println("");
}


// Bytes -> float or double.
void loop2() {
  Serial.println("Converting array of bytes to float or double...");
  for (size_t i = 0; i < sizeof valueOut; i++)
    ((byte *) & valueOut)[i] = ArrayOfBytes[SpecificLocation + i];
}


//
