



double value = 123.456;
unsigned int temp =  0;

void setup() {
  Serial.begin(38400);
  delay(2000);
  byte * valuePtr = (byte *) &value; // we find the address of the first byte
  Serial.print("\nValue uses ");
  Serial.print(sizeof(value));
  Serial.print(" bytes in Memory starting at address 0x");
  Serial.println((uint32_t) valuePtr, HEX);
  for (size_t i = 0; i < sizeof(value); i++) {
    Serial.print(F("Memory address 0x"));
    Serial.print((uint32_t) (valuePtr + i), HEX);
    Serial.print(F("= 0x"));
    Serial.println(*(valuePtr + i), HEX);
  }
}

void loop() {}
