/*
   Testing internal ADC

*/

// Pins
const byte pressureIn =   A1; // Analog voltage from pressure sensor
const byte timerIn =      A4; // Timer output, analog voltage
const byte batteryIn =    A6; // Voltage at power input. Voltage divider at input

// Constants
const long bRate =         38400; // Baud rate
const long dTime =          100; // Delay time
const unsigned int nAvg =  1000;
// Analog - Internal
double press_gain = 0.4166666666;
double press_offset =      -0.25;
//float press_gain = 0.4166666666;
//float press_offset =      -0.25;
float timer_gain =        2.006;
float battery_gain =      7.375;

// Variables
double pressVoltage =    0.0; // Pressure voltage
double pressure =        0.0; // Pressure [mbar]
//float pressVoltage =    0.0; // Pressure voltage
//float pressure =        0.0; // Pressure [bar]
float timerVoltage =    0.0; // Voltage from timer
float battVoltage =     0.0; // Power supply voltage

void setup() {
  Serial.begin(bRate);
  while (!Serial) {
    if (millis() >= 5000) break;
  }
  //  Serial.print("\nStarting up...\n");
  analogReadResolution(12); // Lifting the resolution from 10 to 12 bit
  //  Serial.println("Internal 12-bit ADC init done...\n");
}

void loop() {
  byte monitorType = 1; // 0 = Plotter, 1 = Serial
  //  pressVoltage = 0.6;
  pressVoltage = 0.0;
  for (unsigned int i = 0; i < nAvg; i++) {
    pressVoltage += double(analogRead(pressureIn)) / 4096.0 * 3.3;
  }
  pressVoltage /= double(nAvg);
  pressure = (pressVoltage * press_gain) + press_offset;
  delay(1);
  timerVoltage = (float(analogRead(timerIn)) / 4096.0 * 3.3) * timer_gain;
  delay(1);
  battVoltage = (float(analogRead(batteryIn)) / 4096.0 * 3.3) * battery_gain;
  if (monitorType) {
    Serial.print("Pressure Voltage: ");
    Serial.print(pressVoltage, 7);
    Serial.println(" Volt"); //*/
    Serial.print("Pressure: ");
    Serial.print(pressure * 1000.0, 7);
    Serial.println(" mbar");
    Serial.print("Pressure: ");
    Serial.print(pressure * 29.529987508, 7); // bar -> inHg
    Serial.println(" inHg");
    Serial.print("Vacuum: ");
    Serial.print((1.0 - pressure) * 29.529987508, 7);
    Serial.println(" inHg");
    Serial.print("Timer Voltage: ");
    Serial.print(timerVoltage, 3);
    Serial.println(" Volt");
    Serial.print("Battery Voltage: ");
    Serial.print(battVoltage, 3);
    Serial.println(" Volt\n"); //*/
  }
  else Serial.println(pressure * 1000.0, 7);
  delay(dTime);
}
