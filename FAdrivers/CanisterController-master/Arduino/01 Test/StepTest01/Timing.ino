/*
 * Use millis to time a session
 * 
 */

byte timeIsUp() {
  unsigned long newTime = millis();
  unsigned long diffTime = 0;
  if(newTime > oldTime) diffTime = newTime - oldTime;
  else diffTime = 0xFFFFFFFF - newTime + oldTime + 1;
  if (diffTime >= dTime) {
    oldTime = newTime;
    return 1;
  }
  else return 0;
}


//
