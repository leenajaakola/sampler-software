/*
 * In- and output functions
 *
 */

void ioInit() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  pinMode(valve1Pin, OUTPUT);
  digitalWrite(valve1Pin, LOW);
  pinMode(valve2Pin, OUTPUT);
  digitalWrite(valve2Pin, LOW);
}

void valveUpdate(byte valveIn) {
  if(valveIn) digitalWrite(valve1Pin, HIGH);
  else digitalWrite(valve2Pin, HIGH);
  delay(75);
  digitalWrite(valve2Pin, LOW);
  digitalWrite(valve1Pin, LOW);
}



//
