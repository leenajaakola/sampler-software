/*
   Testing step function without warning and error stuff
   Double types for high resolution

*/

// Pins
const byte ledPin =         3; // On board LED
const byte valve1Pin =      4;
const byte valve2Pin =      5;
const byte pressureIn =    A1; // Analog voltage from pressure sensor
const byte timerOutPin =   A4; // Output from timer
const byte psuVoltagePin = A6; // Voltage from battery

// Constants
const long bRate =                 38400; // Baud rate
const long dTime =                 60000; // Delay time in ms
const unsigned int nAvg =           1000; // Number of pressure measurings to average
const double pressGain =    0.4166666666;
const double pressOffset =         -0.25;
const double pressStep =             2.0; // Step in pressure before closing valve
const double pressClose =  1000.0 - 67.7; // Pressure [mbar] (2 inHg = 67.7mbar)
const double pressLow =            800.0; // Pressure [mbar]
const float timer_gain =             2.0; // No need for precision
const float battery_gain =         7.375;
const unsigned long minutes =         60; // Number of minutes before valve is turned on

// Variables
double pressVoltage =       0.0; // Pressure voltage
double pressure =           0.0; // Pressure [mbar]
double pressureHg =         0.0; // Pressure [mbar]
double pressCalc =          0.0; // Pressure [mbar]
float timerVoltage =        0.0; // Voltage from timer
float battVoltage =         0.0; // Power supply voltage
byte serialOn =               0;
byte runFlag =                0;
unsigned long oldTime =       0;
unsigned long n =             0;
unsigned long minutesN =     59; // Minut counter
byte valveState =             0;

void setup() {
  ioInit();
  Serial.begin(bRate);
  while (!Serial) {
    if (millis() >= 10000) break;
  }
  if (Serial) serialOn = 1;
  else serialOn = 0;
  analogReadResolution(12); // Lifting the resolution from 10 to 12 bit
  if (serialOn) {
    Serial.println("\nDMR Canister logger and timer\n");
    Serial.print("Logging interval\t"); Serial.print(dTime / 1000); Serial.println("\tseconds");
    Serial.print("Pressure step\t"); Serial.print(pressStep); Serial.println("\tmbar\n");
    Serial.println("Log [N]\tPressure [mbar]\tPressure [inHg]\tValve state [0/1]");
  }
  pressUpdate();
  pressCalc = pressure;
  if (pressure < pressLow) {
    runFlag = 1;
  }
  else {
    runFlag = 0;
    valveState = 0;
    valveUpdate(valveState);
  }
  control();
}

void loop() {
  if (serialOn && timeIsUp() && runFlag) {
    control();
  }
}

void control() {
  digitalWrite(ledPin, HIGH);
  pressUpdate();
  if (pressure >= pressClose) {
    runFlag = 0;
    valveState = 0;
    valveUpdate(valveState);
    Serial.println("\n End of Data");
  }
  if (runFlag) {
    minutesN++;
    if (minutesN >= minutes) {
      minutesN = 0;
      pressCalc += pressStep;
      if (!valveState) {
        valveState = 1;
        valveUpdate(valveState);
      }
    }
    if (pressure >= pressCalc && valveState) {
      valveState = 0;
      valveUpdate(valveState);
    }
    printData();
  }
  digitalWrite(ledPin, LOW);
}

void printData() {
  n++;
  Serial.print(n);
  Serial.print("\t");
  Serial.print(pressure, 4);
  Serial.print("\t");
  Serial.print(pressureHg, 4);
  Serial.print("\t");
  valveState = 1;
  Serial.println(byte(valveState));
}

void pressUpdate() {
  pressVoltage = 0.0;
  for (unsigned int i = 0; i < nAvg; i++) {
    pressVoltage += double(analogRead(pressureIn)) / 4096.0 * 3.3;
  }
  pressVoltage /= double(nAvg);
  pressure = (pressVoltage * pressGain) + pressOffset;
  pressureHg = pressure * 29.529987508;
  pressure *= 1000.0; // bar -> mbar
}


//
