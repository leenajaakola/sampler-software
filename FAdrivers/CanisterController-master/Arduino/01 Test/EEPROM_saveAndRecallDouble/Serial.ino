/*
 * Serial monitor functions
 * 
 */

// Serial init
void serialInit() {
  if (debug) {
    Serial.begin(bRate);
    while(!Serial) {
      if(millis() >= 2000) {
        serialOn = 0;
        break;
      }
    }
    if(Serial) serialOn = 1;
  }
  if(serialOn) Serial.println("\nSerial online...\n");
}

// I2C init
void i2cInit() {
  Wire.begin();
  if(serialOn) Serial.println("I2C started\n");
}


//
