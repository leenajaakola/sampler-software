/*
 * EEPROM stuff
 * 
 */

void eepromInit() {
  eeprom= new ZEeprom();
  eeprom->begin(Wire,AT24Cxx_BASE_ADDR,AT24C02);
  if(serialOn) Serial.println("EEPROM Init done");
}

void eepromRead() {
  if(serialOn) Serial.println("Read EEPROM bytes");
  byte data = eeprom->readByte(eeAddress);
  if(serialOn) {
    Serial.print("Read byte = 0x");
    Serial.print(data, HEX);
    Serial.println("");
  }
}

void eepromWrite() {
  if(serialOn) Serial.println("Write EEPROM bytes");
  eeprom= new ZEeprom();
  eeprom->begin(Wire,AT24Cxx_BASE_ADDR,AT24C02);
  if(serialOn) Serial.println("EEPROM Init done");
  eeprom->writeByte(eeAddress, 0xBB); // Write a byte at address 0 in EEPROM memory.
  delay(10); // Write cycle time (tWR). See EEPROM memory datasheet for more details.
  if(serialOn) Serial.println("Read byte from EEPROM memory..."); // Read a byte at address 0 in EEPROM memory.
  byte data = eeprom->readByte(eeAddress);
  if(serialOn) {
    Serial.print("Read byte = 0x");
    Serial.print(data, HEX);
    Serial.println(""); 
  }
}


//
