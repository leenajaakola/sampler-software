/* 
 * Canister pressure step test 
 * 
 * Advarsler og sluk:
 * 10"Hg (339mbar vacuum = 1.0 - 0.339 = 0.661bar)
 * 2"Hg (67.7mbar vacuum = 1.0 - 0.0677 = 0.932bar)
 */

// Debug setting
const byte debug = 1; // 1 = debugging, 0 = not

// Constants
const long bRate =             38400; // Baud rate
const double warnPressure =    661.0; // Pressure [mBar]
const double endPressure =     932.0; // Pressure [mBar]
const long totalDays =            14; // Total hours running
const long totalHours = totalDays*24; // Total hours running

// Variables
byte serialOn =           0;
//Pressure
double pressure =       0.0; // Pressure [mBar]
double startPressure =  0.0; // Pressure [mBar]
double stepPressure =   0.0; // Pressure [mBar]
// Timing
unsigned long tripHour =  0;

void setup() {
  serialInit();
  // Calculating pressure step
  stepPressure = (endPressure - startPressure) / double(totalHours);
  if(serialOn) {
    Serial.print("Total days: ");
    Serial.println(totalDays);
    Serial.print("Total hours: ");
    Serial.println(totalHours);
    Serial.print("Initial pressure [mbar]: ");
    Serial.println(startPressure);
    Serial.print("Final pressure [mbar]: ");
    Serial.println(endPressure);
    Serial.print("Step pressure [mbar]: ");
    Serial.println(stepPressure, 11);
    Serial.println("");
  }
  // Calculating every pressure step and print it
  for(tripHour; tripHour < totalHours; tripHour ++) {
    pressure += stepPressure;
    if(serialOn) {
      Serial.print("Trip hour: ");
      Serial.print(tripHour);
      Serial.print("\tPressure: ");
      Serial.println(pressure, 3);
    }
  }
  Serial.println("\nTHE END...");
}

void loop() {
}


//
