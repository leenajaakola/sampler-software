/*
 * Serial monitor
 * 
 */

// Serial init
void serialInit() {
  if (debug) {
    Serial.begin(bRate);
    while(!Serial) {
      if(millis() >= 2000) {
        serialOn = 0;
        break;
      }
    }
    if(Serial) serialOn = 1;
  }
  if(serialOn) Serial.println("\nPressure step calculator online...\n");
}
