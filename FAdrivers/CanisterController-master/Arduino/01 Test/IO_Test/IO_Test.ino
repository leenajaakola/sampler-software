/*
   IO testing
*/

// Debug setting
const byte debug = 1; // 1 = debugging, 0 = not


// Pins
const byte lcdPowerPin =          5;
const byte sdCardDetect =         6; // SD card detect
const byte sdChipSelect =         7; // SD card chip select
const byte powerOffPin =         A0;
const byte psuVoltagePin =       A1;
const byte timerOutPin =         A2;
const byte ledPin =              A6;
const byte ledOnBoard = LED_BUILTIN;

// Constants
const unsigned int bRate = 38400;

// Variables
byte serialOn =                0;

void setup() {
  serialInit();
  ioInit();
}

void loop() {
  if (!digitalRead(sdCardDetect)) { // SD card inserted?
    if (serialOn) Serial.println("SD card inserted");
    delay(1000);
  }
}
