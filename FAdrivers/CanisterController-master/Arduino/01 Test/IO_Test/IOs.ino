/*
 * IO functions
 * 
 */

void ioInit() {
  if (serialOn) Serial.println("IO init started");
  pinMode(lcdPowerPin, OUTPUT);
  digitalWrite(lcdPowerPin, LOW);
  pinMode(sdCardDetect, INPUT);
  digitalWrite(sdCardDetect, HIGH);
  pinMode(powerOffPin, OUTPUT);
  digitalWrite(powerOffPin, LOW);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  pinMode(ledOnBoard, OUTPUT);
  digitalWrite(ledOnBoard, LOW);
  if (serialOn) Serial.println("IO init done");
}
