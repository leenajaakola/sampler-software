/* Machine "Mother" SMS sender
 *
 * Arduino MKR GSM 1400
 */

// Include the GSM library
#include <MKRGSM.h>

//#include "arduino_secrets.h" 
// Please enter your sensitive data in the Secret tab or arduino_secrets.h
// PIN Number
const char PINNUMBER[] = "3250"; // Leave empty if no PIN number
//const char PINNUMBER[] = "1234"; // Leave empty if no PIN number
//const char PINNUMBER[] = SECRET_PINNUMBER;

// initialize the library instance
GSM gsmAccess;
GSM_SMS sms;

// Debugging
const byte debug =  0; // 1 = debugging, 0 = not

// Pins
const byte testIn = 0; // Unused pin on PCB and is used as test input here
const byte ledPin = 3; // LED on PCB

// Constants
const char remoteNum[] = "004526591373";  // telephone number to send sms
const char txtMsg[] = "Testing";  // telephone number to send sms

// Variables
byte serialOn = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  pinMode(testIn, INPUT);
  digitalWrite(testIn, HIGH); // Activate internal pull-up
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  if (debug) {
    Serial.begin(38400);
    while (!Serial) {
      if(millis() > 5000) break;
    }
  }
  if (Serial) serialOn = 1;
  else serialOn = 0;
  if(serialOn) Serial.println("\nMother SMS service");
  // connection state
  bool connected = false;
  // Start GSM shield
  // If your SIM has PIN, pass it as a parameter of begin() in quotes
  digitalWrite(ledPin, HIGH);
  while (!connected) {
    if(serialOn) Serial.println("\nGSM connecting...");
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      connected = true;
    }
    else {
      if(serialOn) Serial.println("Not connected");
      delay(1000);
    }
    digitalWrite(ledPin, !digitalRead(ledPin));
    delay(100);
  }
  if(serialOn) Serial.println("GSM initialized");
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}

void loop() {
  // sms text
  if(serialOn) {
    Serial.print("Mobile number: ");
    Serial.println(remoteNum);
    Serial.print("Sending message: ");
    Serial.println(txtMsg);
  }
  if (!digitalRead(testIn)) {
    digitalWrite(LED_BUILTIN, HIGH);
    // send the message
    sms.beginSMS(remoteNum);
    sms.print(txtMsg);
    sms.endSMS();
    if(serialOn) Serial.println("COMPLETE!");
  }
  digitalWrite(LED_BUILTIN, LOW);
//  while(1);
}
