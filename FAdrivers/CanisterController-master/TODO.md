# TODO list

* Data der gemmes ved logning:
  * Valve State [0/1], Battery voltage [V]
  * Remove step pressure so we only save Pressure [mbar]
* GSM
* RTC

# Nice to have
* Event monitor on the SD card (the decisions made - maybe the numbers from the flow chart)
* Test framework via serial mon?

# Done
* The TimestampString string is not correct when writing data to the SD card. The format should be: y-m-d h:m:s.
