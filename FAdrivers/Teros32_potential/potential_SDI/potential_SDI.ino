/*
  Recieves SDI signal from TEROS32 
  soil potential sensor and prints to
  the serial monitor. Two or more potentiometers 
  can be connected to the same digital pin, if 
  addresses are changed one at a time using
  Examples -> SDI-12 -> b_change_address
  
  Hardware:
  Arduino Mega/ Uno or MKR board with external power source (5-12V)
  METER group TEROS32 soil water potential sensor
  DPS310 High Precision Barometer Pressure and Altitude Sensor

  TEROS32 wiring scheme: 
  power (brown) 
  ground (bare) 
  digital (orange)

  Author: Leena Jaakola
  @file SDI-> d_simple_logger.ino
  @file SDI-12 -> b_change_address.ino
  https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
  
 */

#include <Dps310.h>                                               // Grove high precision barometer library
#include <SDI12.h>

#define DATA_PIN 1                                               // pin of the SDI bus
Dps310 Dps310PressureSensor = Dps310();
//float a_temp;
//float b_pressure;
//uint8_t oversampling = 3;                                         // oversampling can be a value from 0 to 7, allows higher precision
//uint16_t ret;
SDI12 mySDI12(DATA_PIN);
  
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000};
uint8_t numSensors = 0;

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Dps310PressureSensor.begin(Wire);                               // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                           // Default I2C address 0x76
  Serial.println("Opening SDI-12 bus");
  mySDI12.begin();
  delay(500); 

  Serial.println("Scanning all addresses, please wait...");
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  for (byte i = 'a'; i <= 'z'; i++)                               // scan address space a-z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  for (byte i = 'A'; i <= 'Z'; i++)                               // scan address space A-Z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }

  if (!found) {
    Serial.println(
      "No SDI sensors found");
    while (true) { delay(10); }
  }

  Serial.println();
  Serial.println(
    "Time Elapsed (s), Sensor Address and ID, Total Pressure [kPa], Soil Temperature [degC], meta, Air Temperature [degC], Barometric Pressrue [Pa]");
}

void loop() {
  
  for (char i = '0'; i <= '9'; i++)                                            // scan address space 0-9
    if (isTaken(i)) {
      Serial.print(millis() / 1000); Serial.print(","); printInfo(i); Serial.print(","); takeMeasurement(i); Serial.print(",");
    }
  for (char i = 'a'; i <= 'z'; i++)                                            // scan address space a-z
    if (isTaken(i)) {
      Serial.print(millis() / 1000); Serial.print(","); printInfo(i); Serial.print(","); takeMeasurement(i); Serial.print(",");
    }

  for (char i = 'A'; i <= 'Z'; i++)                                            // scan address space A-Z
    if (isTaken(i)) {
      Serial.print(millis() / 1000); Serial.print(","); printInfo(i); Serial.print(","); takeMeasurement(i); Serial.print(",");
    };

//  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);            // DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision
//  Serial.print(a_temp); Serial.print(",");
//  
//  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
//  Serial.println(b_pressure);
  Serial.println();
  delay(10000);                                                                // wait 10 seconds between measurements 
}



//////////////////////
// Functions ////////
/////////////////////

byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    delay(50);
  }
  Serial.print(buffer);

}

void printInfo(char i) {                                               // i a character between '0'-'9', 'a'-'z', or 'A'-'Z'
  String command = "";
  command += (char)i;
  command += "I!";
  mySDI12.sendCommand(command);
  delay(30);
  printBufferToScreen();
}

bool takeMeasurement(char i) {
  String command = "";
  command += i;
  command += "M!";                                                      // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  delay(30);

  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  Serial.print(sdiResponse); Serial.print(", "); Serial.print(wait); Serial.print(", ");

  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect
  Serial.print(numMeasurements); Serial.print(", ");

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available())                                            // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  delay(30);
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out

  command = "";
  command += i;
  command += "D0!";                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}     
  delay(300);                               
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
