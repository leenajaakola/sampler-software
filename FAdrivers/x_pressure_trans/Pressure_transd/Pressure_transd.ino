/*
 * 
 * Sketch for controlling a diaphram pump and pressure transducer
 * 
 * Hardware:
 * Arduino
 * Transistor (mosfet) for controlling high V and current power source from the low current output of the Arduino (digital switch)
 * 12V power source
 * Gardner Denver 1420VP DC Diaphram pump - Pin 7
 * GEMS 3500 Pressure Transducer - Pin 6
 * 4-20mA
 * 
 * Author: Leena Jaakola
 * Date created: 01-10-2020
 * 
 */
const int trans_low = 193;                                          // 3.3V/158ohm = 887; 5V/220ohm = 193
const int trans_high = 887;                                      

float pumpet;
unsigned long tstart;
unsigned long tstop;

const int transPin = A2;                                            // analog port til pressure transducer
float  trans;
int16_t vakuumvar;

void setup() {
    Serial.begin(9600);
}

void loop() {
    
  trans = analogRead(transPin);               
  vakuumvar = map(trans, trans_low, trans_high, 9, 996);            // to 0-1000 mbar // 250 ohm recommended.. 220ohm -> 0.88-4.4V 
  Serial.print(trans);  Serial.print("\t");  Serial.println(vakuumvar);
  delay(500);
}
