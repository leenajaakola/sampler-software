/*
 *Collects CO2 concentration from K30 sensor and sends average values to Serial Monitor
 *https://www.edaphic.com.au/wp-content/uploads/2015/07/ESSE-18-K30-CO2-Sensor-Manual.pdf?x14766
 *
 *OUT2 - Pin 13
 *1-5V = 0-2000 ppm CO2
 *
 *Disable automatic baseline calibration 
 *calibration? 400ppm
 *error reduced <5ppm by: 
 *  individual calibration
 *  temp/pressure/RH correction
 *  average over 200s
 *  
 *cite for temp/humidity/pressure correction:   
 *  KAZAN, Filiz, "MODELING AND DEVELOPMENT OF THE DYNAMIC ENVIRONMENTAL SIMULATION 
    CHAMBER (DESC) FOR CALIBRATION OF AIR QUALITY MONITORING SENSORS" (2019). Graduate 
    Theses, Dissertations, and Problem Reports. 4089. 
    https://researchrepository.wvu.edu/etd/4089  p. 41
 *  
 *Author: Leena Jaakola
 *
 */

#define co2_Analog 13                                      // CO2 analog pin

int co2_Analogread;
int co2_mV;
float co2_ppm;

void setup() {
  Serial.begin(9600);                                      // Initialize serial monitor
}

void loop() {
  co2_Analogread = analogRead(co2_Analog);
  co2_mV = map(co2_Analogread, 0, 1023, 1000, 5000);
  co2_ppm = map(co2_mV, 1000, 5000, 0, 2000);
  
  Serial.print(co2_Analogread);
  Serial.print("\t"); 
  Serial.print(co2_mV);
  Serial.print("\t"); 
  Serial.println(co2_ppm);
  
  delay(1000);
}
