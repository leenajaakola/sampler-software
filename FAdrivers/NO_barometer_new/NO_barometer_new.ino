/*
  Hardware:
  MKR Board
  Alphasense NO-B4 Nitric Oxide Sensor 4-Electrode

  Reads and logs NO gas concentration
  cite: https://forum.arduino.cc/index.php?topic=376338.0
        https://forum.arduino.cc/index.php?topic=533992.0
        https://zueriluft.ch/makezurich/AAN803.pdf
        https://electronics.stackexchange.com/questions/48842/how-to-read-values-from-a-no2-sensor-from-an-arduino

  Pins: VIN 
        GND
        OP1 = sensor 1 (SN1) working electrode
        OP2 = sensor 2 (SN2) auxiliary electrode
  ISB gain = 0.8 mV/nA
  OBS noise 15 ppb

  Author: Leena Jaakola
  Created: 30-07-2020
  
*/

#include <Dps310.h>                                        // Grove high precision barometer library
#define NO_WE_Analog A2                                    // Analog 2
#define NO_Aux_Analog A2                                   // to GND

Dps310 Dps310PressureSensor = Dps310();

float temperature;
float pressure;
int16_t baro; 
float NO_WE;                                              // uncorrected raw working electrode
float NO_Aux;                                             // uncorrected raw auxiliary electrode
float NO_offset_WE = 545;                                 // zero offset mV
float NO_offset_Aux = 510;                                 // zero offset mV
float NO_WE_offsetT = 362.3;                              // total zero offset mV
float NO_Aux_offsetT = 362.3;                             // total zero offset mV
float NO_WE_e_offset;           // MEASURE open circuit voltage to determine the WE and AE offsets of the ISB
float NO_Aux_e_offset;
float NO_Sensitivity = 520;                               // 0.5-0.85 (varies with temperature)
float temp_n;                                             // temperature correction                            
float NO;                                                 // calculated NO [ppb] 

void setup() {
  Serial.begin(9600);                                     // Initialize serial monitor

  Dps310PressureSensor.begin(Wire);                       // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                   // Default I2C address 0x76

  delay(100);
}

void loop() {

  baro = Dps310PressureSensor.measureTempOnce(temperature);

  if(temperature <= -20.0) {                                            // temperature correction
    temp_n = 1.8;
  }
  if(-20.0 < temperature <= -10.0) {
    temp_n = 1.4;
  }
  if(0.0 < temperature <= 10.0) {
    temp_n = 1.1;
  }
  if(10.0 < temperature <= 20.0) {
    temp_n = 1.0;
  }
  if(20.0 < temperature <= 40.0) {
    temp_n = 0.9;
  } 
  else{
    temp_n = 0.8;
  }

  NO_WE = map(analogRead(NO_WE_Analog), 0, 1023, 0, 5000);
  NO_Aux = map(analogRead(NO_Aux_Analog), 0, 1023, 0, 5000);
  
  NO_WE_e_offset = NO_WE_offsetT - NO_offset_WE;                      // calculate electronic offset of ISB
  NO_Aux_e_offset = NO_Aux_offsetT - NO_offset_Aux;
  
  NO = ((NO_WE - NO_WE_e_offset) - (temp_n * (NO_offset_WE / NO_offset_Aux)* (NO_Aux - NO_Aux_e_offset)) / NO_Sensitivity;    // ppb
  
  Serial.println(NO, 3);
  
  delay(1000);
}
