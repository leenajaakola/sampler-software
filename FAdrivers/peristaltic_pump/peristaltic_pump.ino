/*
 * Peristaltic pump
 * Turns on pump for 2 seconds every 2 seconds
 * 5V = 47mL/min
 * 6V = 60mL/min
 * 
 * Author: Leena Jaakola
 * Date created: 27-08-2020
 * 
 * Sources:
 * http://www.learningaboutelectronics.com/Articles/Peristaltic-pump-circuit-with-an-arduino-microcontroller.php#:~:text=The%20collector%20of%20the%20transistor,simply%20gets%20connected%20to%20ground.
 */

const int pp = 6;                           // Peristaltic Pump connected to digital pin 6

void setup() {
  pinMode(pp, OUTPUT);                      // sets the digital pin as output
  digitalWrite(pp, LOW);
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  digitalWrite(pp, HIGH);
  delay(2000);
  digitalWrite(pp, LOW);
  delay(2000);
}
