
/*  
 *   
  Hardware:
  Arduino Mega 2560 (SDI library not compatible with MKR boards)
  METER group TEROS32 soil water potential sensor
  METER group TEROS10 soil volumetric water content sensor
  DPS310 High Precision Barometer Pressure and Altitude Sensor
  Sparkfun SD level shifting breakout

  Collects data from Teros32, Teros10, DPS310, parses data and
  stores on SD breakout
  
  Teros10 A0
  Teros32 digital (orange) pin 10
  DPS310 SDA, SCL 
  SD:
  SCK to 52
  DO to 50
  DI to 51
  CS to 53
  CD to 48

  Author:      Leena Jaakola

  Created: 18-09-2020

  Sources:     @file d_simple_logger.ino 
               https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
               https://iot-guider.com/arduino/serial-communication-between-two-arduino-boards/
               https://thewanderingengineer.com/2015/05/06/sending-16-bit-and-32-bit-numbers-with-arduino-i2c/#
               https://forum.arduino.cc/index.php?topic=592795.0

  TROUBLESHOOT:
  -The only way it works: upload C:\Users\LJ\OneDrive - Københavns Universitet\Arduino\ASS\Teros32_potential/potential_SDI 
  Re-upload this script to the Arduino.
  Plug in the external power source to VIN
  Unplug from computer 
  -Low power messes with I2C (barometer) readings
  -Potentiometer readings don't change
  
  
 */

#include <Dps310.h>                                               // Grove high precision barometer library
#include <SDI12.h>
#include <SD.h>                                                   // SD library
#include <LowPower.h>

// CHANGE FILE NAME // 
String logFile = "log-0010.txt";

#define DATA_PIN 10                                              // digital pin of the SDI bus compatible on Mega 2560 with pins: 0, 11, 12, 13, 14, 15, 50, 51, 52, 53, A8 (62), A9 (63), A10 (64), A11 (65), A12 (66), A13 (67), A14 (68), A15 (69)
Dps310 Dps310PressureSensor = Dps310();
const uint8_t chipSelect = 53;
const uint8_t cardDetect = 48;
const int moistureAnalogIn = A0;                                  // TEROS10 orange analog wire
int moistureAnalog;
float moisturemV;                                             
float vwc;
float a_temp;
float b_pressure;
int ind1;                                                         // , locations
int ind2;
int ind3;
int ind4;
String blank;
String t_pressure_str;
String s_temp_str;
String meta_str;
float t_pressure;
float s_temp;
int meta;
int oversampling = 3;                                             // oversampling can be a value from 0 to 7, allows higher precision
uint16_t ret;
String sdiString = "";                                            // save SDI data to a string
SDI12 mySDI12(DATA_PIN);  
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000};
uint8_t numSensors = 0;
bool SDAvailable = false;

void setup() {
  Serial.begin(115200);
  
  while (!Serial);
  // initialize SD card:
  SDAvailable = SD.begin(chipSelect);
  Serial.println("Card working: " + String(SDAvailable));
  // print a header to the SD card file:
  File dataFile = SD.open(logFile, FILE_WRITE);
  if (dataFile) {
  dataFile.println("Time Stamp, VWC θ [m^3/m^3], Air Temp [C], Ψa [Pa], Ψt [Pa], Ψm [Pa], Soil Temp [C], meta");
  dataFile.close();
  }
                                    
  Dps310PressureSensor.begin(Wire);                               // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                           // Default I2C address 0x76

  mySDI12.begin();
  Serial.println("Opening SDI-12 bus");
  delay(500); 
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'a'; i <= 'z'; i++)                               // scan address space a-z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'A'; i <= 'Z'; i++)                               // scan address space A-Z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }
  if (!found) {
    Serial.println("No SDI sensors found");
    }
  Serial.println();

}

void loop() {

  
  
  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, 0, 1023, 0, 5000);                          // Map to range of analog output to mV
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3) 
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;                                // Third-order calibration equation mV to VWC
  
  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);            // DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision
  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
  
  for (char i = '0'; i <= '9'; i++)                                            // scan address space 0-9
    if (isTaken(i)) {
      takeMeasurement(i);
    }
  for (char i = 'a'; i <= 'z'; i++)                                            // scan address space a-z
    if (isTaken(i)) {
      takeMeasurement(i);
    }
  for (char i = 'A'; i <= 'Z'; i++)                                            // scan address space A-Z
    if (isTaken(i)) {
      takeMeasurement(i);
    };

  ind1 = sdiString.indexOf(',');                                        // parse SDI data; locates first comma
  blank = sdiString.substring(0, ind1);
  ind2 = sdiString.indexOf(',', ind1+1);
  t_pressure_str = sdiString.substring(ind1+1, ind2+1);
  t_pressure_str.remove(7);                                             // remove "," from string
  ind3 = sdiString.indexOf(',', ind2+1);
  s_temp_str = sdiString.substring(ind2+1, ind3+1);
  s_temp_str.remove(4);
  ind4 = sdiString.indexOf(',', ind3+1);
  meta_str = sdiString.substring(ind3+1);
  t_pressure = t_pressure_str.toFloat();
  s_temp = s_temp_str.toFloat();  
  
  if (SDAvailable) {
  File dataFile = SD.open(logFile, FILE_WRITE);
  dataFile.print(vwc);
  dataFile.print (",");
  dataFile.print(a_temp);
  dataFile.print(",");
  dataFile.print(b_pressure);
  dataFile.print(",");
  dataFile.print(t_pressure, 4);
  dataFile.print(",");
  dataFile.println(s_temp, 2);
  dataFile.close();
  
  Serial.print("VWC: "); Serial.println(vwc);
  Serial.print("Air temp: "); Serial.println(a_temp);
  Serial.print("Barometric Pressure: "); Serial.println(b_pressure);
  Serial.print("Total Pressure: "); Serial.println(t_pressure, 4);
  Serial.print("Soil Temp: "); Serial.println(s_temp, 2);
  Serial.print("meta: "); Serial.println(meta);
  Serial.println();
}

  delay(10000);                                                       // wait 10 seconds between measurements 
}





/////////////////////////
///// Functions ////////
////////////////////////



byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } 
    else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    sdiString = String(buffer);
    delay(50);
  }
  //Serial.println(buffer);
}

bool takeMeasurement(char i) {
  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect
  
  delay(30);
  
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out
  String command = "";
  command += i;
  command += "D0!";                                                     // SDI-12 measurement command format  [address]['D0'][!]                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}     
  delay(300);                               
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
