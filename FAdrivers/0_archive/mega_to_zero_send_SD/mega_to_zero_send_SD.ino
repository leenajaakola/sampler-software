

/*
 
  Hardware:
  Arduino Mega 2560 (collects and transmits data)
  Arduino MKRZero (receives data)
  METER group TEROS32 soil water potential sensor
  METER group TEROS10 soil volumetric water content sensor
  DPS310 High Precision Barometer Pressure and Altitude Sensor
  DS1307 RTC

  Wire Master Writer: Mega2560
  Writes to an I2C slave MKRZero board
  Use with C:\Users\LJ\OneDrive - Københavns Universitet\Arduino\ASS\4_data_mega_to_zero_recieve_RTC
  
  Two boards: SDA to SDA, SCL to SCL, GND to GND, power two boards separately via USB
  Teros10 A0
  Teros32 digital pin 11
  DPS310 SDA, SCL 
  RTC SDA, SCL
  
  TEROS wiring scheme: 
               power (brown) 
               ground (bare) 
               data (orange)

  Author:      Leena Jaakola

  Created: 28-07-2019

  Sources:     @file d_simple_logger.ino 
               https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
               https://iot-guider.com/arduino/serial-communication-between-two-arduino-boards/
               https://thewanderingengineer.com/2015/05/06/sending-16-bit-and-32-bit-numbers-with-arduino-i2c/#
               https://forum.arduino.cc/index.php?topic=592795.0
 */

#include <Wire.h>                                                 // I2C Library
#include <Dps310.h>                                               // Grove high precision barometer library
#include <SDI12.h>
#include <SD.h>                                                   // SD library

#define DATA_PIN A15                                               // digital pin of the SDI bus compatible on Mega 2560 with pins: 0, 11, 12, 13, 14, 15, 50, 51, 52, 53, A8 (62), A9 (63), A10 (64), A11 (65), A12 (66), A13 (67), A14 (68), A15 (69)
Dps310 Dps310PressureSensor = Dps310();
const int  SD_CHIP_SELECT = 28; // SDCARD_SS_PIN;

const int moistureAnalogIn = A0;                                  // TEROS10 orange analog wire
int moistureAnalog;
float moisturemV;                                             
float vwc;
float a_temp;
float b_pressure;
int ind1;                                                         // , locations
int ind2;
int ind3;
int ind4;
String blank;
String t_pressure_str;
String s_temp_str;
String meta_str;
float t_pressure;
float s_temp;
int meta;
int oversampling = 3;                                             // oversampling can be a value from 0 to 7, allows higher precision
uint16_t ret;
String sdiString = "";                                            // save SDI data to a string
SDI12 mySDI12(DATA_PIN);  
byte addressRegister[8] = {0B00000000, 0B00000000, 0B00000000, 0B00000000,              // active addresses
                           0B00000000, 0B00000000, 0B00000000, 0B00000000};
uint8_t numSensors = 0;
byte table[12];                                                   // for wire transfer of data between Arduinos (I2C)s
bool SDAvailable = false;
String logFile = "DATALOG.CSV";

void setup() {
  Serial.begin(115200);
  
  while (!Serial);
  // initialize SD card:
  SDAvailable = SD.begin(SD_CHIP_SELECT);
  Serial.println("Card working: " + String(SDAvailable));
  // print a header to the SD card file:
  File dataFile = SD.open(logFile, FILE_WRITE);
  if (dataFile) {
  dataFile.println("Time Stamp, VWC θ [m^3/m^3], Air Temp [C], Ψa [Pa], Ψt [Pa], Ψm [Pa], Soil Temp [C], meta");
  dataFile.close();
  }
  
  Wire.begin(11);                                                 // join I2C bus with address #11 (corresponding to SDA pin on MKRZero)
  Wire.onRequest(requestEvent);                                   
  Dps310PressureSensor.begin(Wire);                               // Initialize DPS310 default I2C address
  Serial.println("DPS310 Initialized");                           // Default I2C address 0x76

  mySDI12.begin();
  Serial.println("Opening SDI-12 bus");
  delay(500); 
  for (byte i = '0'; i <= '9'; i++)                               // scan address space 0-9
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'a'; i <= 'z'; i++)                               // scan address space a-z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  
  for (byte i = 'A'; i <= 'Z'; i++)                               // scan address space A-Z
    if (checkActive(i)) {
      numSensors++;
      setTaken(i);
    }  

  boolean found = false;                                          // scan for active sensors
  for (byte i = 0; i < 62; i++) {
    if (isTaken(i)) {
      found = true;
      Serial.print("First address found:  ");
      Serial.println(decToChar(i));
      Serial.print("Total number of sensors found:  ");
      Serial.println(numSensors);
      break;
    }
  }
  if (!found) {
    Serial.println("No SDI sensors found");
    }
  Serial.println();

}

void loop() {
  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, 0, 1023, 0, 5000);                          // Map to range of analog output to mV
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3) 
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;                                // Third-order calibration equation mV to VWC
  
  ret = Dps310PressureSensor.measureTempOnce(a_temp, oversampling);            // DPS310 performs 2^oversampling internal temperature measurements and combine them to one result with higher precision
  ret = Dps310PressureSensor.measurePressureOnce(b_pressure, oversampling);
  
  for (char i = '0'; i <= '9'; i++)                                            // scan address space 0-9
    if (isTaken(i)) {
      takeMeasurement(i);
    }
  for (char i = 'a'; i <= 'z'; i++)                                            // scan address space a-z
    if (isTaken(i)) {
      takeMeasurement(i);
    }
  for (char i = 'A'; i <= 'Z'; i++)                                            // scan address space A-Z
    if (isTaken(i)) {
      takeMeasurement(i);
    };

  ind1 = sdiString.indexOf(',');                                        // parse SDI data; locates first comma
  blank = sdiString.substring(0, ind1);
  ind2 = sdiString.indexOf(',', ind1+1);
  t_pressure_str = sdiString.substring(ind1+1, ind2+1);
  t_pressure_str.remove(7);                                             // remove "," from string
  ind3 = sdiString.indexOf(',', ind2+1);
  s_temp_str = sdiString.substring(ind2+1, ind3+1);
  s_temp_str.remove(4);
  ind4 = sdiString.indexOf(',', ind3+1);
  meta_str = sdiString.substring(ind3+1);
  t_pressure = t_pressure_str.toFloat();
  s_temp = s_temp_str.toFloat();  
  
  if (SDAvailable) {
  File dataFile = SD.open(logFile, FILE_WRITE);
  dataFile.print(vwc);
  dataFile.print (",");
  dataFile.print(a_temp);
  dataFile.print(",");
  dataFile.print(b_pressure);
  dataFile.print(",");
  dataFile.print(t_pressure);
  dataFile.print(",");
  dataFile.println(s_temp);
  dataFile.close();
}
    
  delay(10000);                                                       // wait 10 seconds between measurements 
}





/////////////////////////
///// Functions ////////
////////////////////////

void requestEvent() {                                                 // Wire I2C
  float vwc_hun = vwc * 100;
  int8_t vwc_int = vwc_hun;
  
  float a_temp_ti = a_temp * 10;
  int8_t a_temp_int = a_temp_ti;
  
  float b_pressure_hun = b_pressure * 100;
  int32_t b_pressure_int = b_pressure_hun;

  float t_pressure_tusen = t_pressure * 1000;
  int32_t t_pressure_int = t_pressure_tusen;

  float s_temp_ti = s_temp * 10;
  int s_temp_int = s_temp_ti;
  
  table[0] = vwc_int & 0xFF;
  table[1] = a_temp_int & 0xFF;
  table[2] = (b_pressure_int >> 24) & 0xFF;                           // atm pressure pt 1
  table[3] = (b_pressure_int >> 16) & 0xFF;                           // atm pressure pt 2
  table[4] = (b_pressure_int >> 8) & 0xFF;                            // atm pressure pt 3
  table[5] = b_pressure_int & 0xFF;                                   // atm pressure pt 4
  table[6] = (t_pressure_int >> 24) & 0xFF;                           // teros32 pressure pt 1
  table[7] = (t_pressure_int >> 16) & 0xFF;                           // teros32 pressure pt 2
  table[8] = (t_pressure_int >> 8) & 0xFF;                            // teros32 pressure pt 3
  table[9] = t_pressure_int & 0xFF;                                   // teros32 pressure pt 4
  table[10] = s_temp_int & 0xFF;                                      // teros32 soil temp
  table[11] = meta & 0xFF;                                            // teros32 meta status
  Wire.write(table, 12);
  
  Serial.print("VWC: "); Serial.println(vwc);
  Serial.print("Air temp: "); Serial.println(a_temp);
  Serial.print("Barometric Pressure: "); Serial.println(b_pressure);
  Serial.print("Total Pressure: "); Serial.println(t_pressure_int);
  Serial.print("Soil Temp: "); Serial.println(s_temp);
  Serial.print("meta: "); Serial.println(meta);
  Serial.println();
  
  delay(1000);
}

byte charToDec(char i) {                                                       // converts allowable address characters ('0'-'9', 'a'-'z', 'A'-'Z') to a decimal number between 0 and 61
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z'))
    return i - 'A' + 37;
  else
    return i;
}

char decToChar(byte i) {
  if (i <= 9) return i + '0';
  if ((i >= 10) && (i <= 36)) return i + 'a' - 10;
  if ((i >= 37) && (i <= 62))
    return i + 'A' - 37;
  else
    return i;
}

void printBufferToScreen() {
  String buffer = "";
  mySDI12.read();
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+') {
      buffer += ',';
    } 
    else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    sdiString = String(buffer);
    delay(50);
  }
  //Serial.println(buffer);
}

bool takeMeasurement(char i) {
  String sdiResponse = "";                                              // [address][ttt (3 char, seconds)][number of measurements available, 0-9]
  delay(30);
  while (mySDI12.available())
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  uint8_t wait = 0;                                                     // find out how long to wait (seconds)
  wait = sdiResponse.substring(1, 4).toInt();
  int numMeasurements = sdiResponse.substring(4, 5).toInt();            // Set up the number of results to expect
  
  delay(30);
  
  mySDI12.clearBuffer();                                                // Wait for anything else and clear it out
  String command = "";
  command += i;
  command += "D0!";                                                     // SDI-12 measurement command format  [address]['D0'][!]                                                     // aD0! a+<matricPotential>±<temperature>+<meta>    // aD1! a±<pitch>±<roll>
  mySDI12.sendCommand(command);                                         // (meta 0 = no sensor error, 1 = temps below freezing, 16 = sensor refill orientation error, 17 = both 1 and 16)
  while (!(mySDI12.available() > 1)) {}     
  delay(300);                               
  printBufferToScreen();
  mySDI12.clearBuffer();
  return true;
}

boolean checkActive(char i) {                                           // this checks for activity at a particular address, expects '0'-'9', 'a'-'z', or 'A'-'Z'
  String myCommand = "";
  myCommand        = "";
  myCommand += (char)i;                                                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {                                         // three contact attempts
    mySDI12.sendCommand(myCommand);
    delay(30);
    if (mySDI12.available()) {
      printBufferToScreen();
      mySDI12.clearBuffer();
      return true;
    }
  }
  mySDI12.clearBuffer();
  return false;
}

boolean isTaken(byte i) {                                               // check if the address has already been taken by an active sensor
  i      = charToDec(i);                                                // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;                                                       // byte #
  byte k = i % 8;                                                       // bit #
  return addressRegister[j] & (1 << k);                                 // return bit status
}

boolean setTaken(byte i) {                                              // sets the bit in the proper location within the address
  boolean initStatus = isTaken(i);
  i                  = charToDec(i);                                    // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j             = i / 8;                                           // byte #
  byte k             = i % 8;                                           // bit #
  addressRegister[j] |= (1 << k);                                       // Register to record that the sensor is active and the address is taken
  return !initStatus;                                                   // return false if already taken
}
