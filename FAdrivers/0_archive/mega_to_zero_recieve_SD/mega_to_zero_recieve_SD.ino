
/*
 * Hardware: 
  Arduino Mega 2560 (collects and transmits data)
  Arduino MKRZero (receives data)
  METER group TEROS32 soil water potential sensor
  METER group TEROS10 soil volumetric water content sensor
  DPS310 High Precision Barometer Pressure and Altitude Sensor
  DS1307 RTC
 * 
 * Wire Slave Receiver: MKRZero
 * Receives data as an I2C slave device and logs data on a micro SD card
 * Use with C:\Users\LJ\OneDrive - Københavns Universitet\Arduino\ASS\4_data_mega_to_zero_send
 * Connect SDA to SDA, SCL to SCL, GND to GND, power two boards separately (Mega runs at 5V, Zero at 3.3V)
 * 
 * Author: Leena Jaakola
 * 
 * Created 28-07-2019
 * 
  Sources:     @file d_simple_logger.ino 
               https://learn.adafruit.com/adafruit-dps310-precision-barometric-pressure-sensor/arduino
               https://iot-guider.com/arduino/serial-communication-between-two-arduino-boards/
               https://thewanderingengineer.com/2015/05/06/sending-16-bit-and-32-bit-numbers-with-arduino-i2c/#
               
 */
 
#include <Wire.h>                                                 // I2C Library
#include <DS1307.h>                                               // Grove RTC

byte a, b, c, d, e, f, g, h, i, j, k, l;
int vwc_int;
int vwc_adj;
int a_temp_int;
int32_t b_pressure_int;
int32_t t_pressure_int;
int s_temp_int;
float vwc;
float a_temp;
float b_pressure;
float t_pressure;
float s_temp;
int meta;
float m_pressure;

DS1307 clock;                                               // object for DS1307 class 

void setup() {
  Wire.begin();
  
  Serial.begin(115200);

  // "Time Stamp, VWC θ [m^3/m^3], Air Temp [C], Ψa [Pa], Ψt [Pa], Ψm [Pa], Soil Temp [C], meta"
  clock.begin();
  clock.fillByYMD(2020,07,28);                                    // SET DATE AND TIME or buy a CR1225 battery :) 
  clock.fillByHMS(17,26,00);
  clock.setTime();
}

void loop() {

  Wire.requestFrom(11, 12);           // request 12 bytes from slave device #11
  a = Wire.read();                    // vwc
  b = Wire.read();                    // air temp a_temp
  c = Wire.read();                    // b_pressure barometric pressure pt1
  d = Wire.read(); 
  e = Wire.read(); 
  f = Wire.read(); 
  g = Wire.read();                    // Teros32 total pressure t_pressure
  h = Wire.read(); 
  i = Wire.read(); 
  j = Wire.read(); 
  k = Wire.read();                    // Teros32 soil temp s_temp
  l = Wire.read();                    // Teros32 meta status
  
  vwc_int = a;
  a_temp_int = b;
  b_pressure_int = c;
  b_pressure_int = (b_pressure_int << 8) | d;
  b_pressure_int = (b_pressure_int << 8) | e;
  b_pressure_int = (b_pressure_int << 8) | f;
  t_pressure_int = g;
  t_pressure_int = (t_pressure_int << 8) | h;
  t_pressure_int = (t_pressure_int << 8) | i;
  t_pressure_int = (t_pressure_int << 8) | j;
  s_temp_int = k;
  meta = l;
  
  if(vwc_int > 127) {                 // adjust bytes to +/- integer
    vwc_adj = 256 - vwc_int;
    vwc_adj = -vwc_adj;
  }
  else {
    vwc_adj = vwc_int;
  }
  
  vwc = float(vwc_adj / 100.00);
  a_temp = float(a_temp_int / 10.00);
  b_pressure = float(b_pressure_int / 100.00);
  s_temp = float(s_temp_int / 10.00);
  t_pressure = float(t_pressure_int);
  m_pressure = t_pressure - b_pressure;
  
  printTime(); Serial.print(",");                                 // print date and time from RTC
  Serial.print(vwc); Serial.print(","); 
  Serial.print(a_temp); Serial.print(","); 
  Serial.print(b_pressure); Serial.print(","); 
  Serial.print(t_pressure); Serial.print(",");
  Serial.print(m_pressure); Serial.print(",");
  Serial.print(s_temp); Serial.print(",");
  Serial.println(meta); 
  
  delay(10000);                                                   // 10 s between measurements
}

void printTime()  {
  clock.getTime();
  Serial.print(clock.year + 2000, DEC); Serial.print("-");
  Serial.print(clock.month, DEC); Serial.print("-");
  Serial.print(clock.dayOfMonth, DEC); Serial.print(" ");
  Serial.print(clock.hour, DEC); Serial.print(":");
  Serial.print(clock.minute, DEC); Serial.print(":");
  Serial.print(clock.second, DEC);
}
