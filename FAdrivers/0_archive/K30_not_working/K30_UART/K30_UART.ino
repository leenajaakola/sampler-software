/*
 Reads K30 CO2 sensor and sends values to Serial Monitor via UART

 Leena Jaakola

 K30  MegaPin
 RX   12
 TX   13
 
*/

/*   AN-126 Example 2 uses kseries.h 
 Reports values from a K-series sensor back to the computer 
 written by Jason Berger 
 Co2Meter.com 
*/ 


#include "kSeries.h" //include kSeries Library
kSeries K_30(12,13); //Initialize a kSeries Sensor with pin 12 as Rx and 13 as Tx
void setup()
{
 Serial.begin(9600); //start a serial port to communicate with the computer
}
void loop()
{
 double co2 = K_30.getCO2('p'); //returns co2 value in ppm ('p') or percent ('%')

 Serial.print(co2); Serial.println("ppm");
 Serial.println();
 delay(1500); 
}
