/*

  Receives from serial port 1, sends to the main serial (Serial 0).

  Works only with boards with more than one serial. (RX, TX) = Mega(19,18); MKR(13,14)

  Teros10 A0
  Teros32 digital (orange) to Mega RX1 pin 19
  DPS310 SDA, SCL 
  SD:
  SCK to 52
  DO to 50
  DI to 51
  CS to 53
  CD to 48

  Only works by removing from power and repowering,
  as Teros32 only takes a Serial reading right
  after powering up
  
*/

//#include <SoftwareSerial.h>
//SoftwareSerial softSerial(18, 19);

void setup() {
  Serial.begin(1200);
  Serial1.begin(1200);
}

void loop() {
  if (Serial1.available()) {
    int inByte = Serial1.read();
    Serial.write(inByte);
  }
}
