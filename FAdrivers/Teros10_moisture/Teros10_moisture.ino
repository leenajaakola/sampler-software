//int dry = 325;
//int sat = 710;
//int dry2 = 330;
//int sat2 = 710;

const int moistureAnalogIn = A0;                                  // TEROS10 orange analog wire
const int moistureAnalogIn2 = A3;
int moistureAnalog = 0;
int moistureAnalog2 = 0;
float moisturemV = 0;   
float moisturemV2 = 0;                                       
float vwc;
float vwc2;

void setup() {
  Serial.begin(9600);

}

void loop() {
  moistureAnalog = analogRead(moistureAnalogIn);
  moisturemV = map(moistureAnalog, 0, 1023, 1000, 2500);                       // Map analog output to 1000-2500 mV
  vwc = 4.824 * pow(10, -10) * pow(moisturemV, 3)                              // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV, 2) 
    + 3.898 * pow(10, -3) * moisturemV - 2.154;   

  moistureAnalog2 = analogRead(moistureAnalogIn2);
  moisturemV2 = map(moistureAnalog2, 0, 1023, 1000, 2500);                       // Map analog output to 1000-2500 mV
  vwc2 = 4.824 * pow(10, -10) * pow(moisturemV2, 3)                              // Third-order calibration equation mV to VWC
    - 2.278 * pow(10, -6) * pow(moisturemV2, 2) 
    + 3.898 * pow(10, -3) * moisturemV2 - 2.154; 
    
Serial.print("Unicorn: "); Serial.print(moistureAnalog); Serial.print("\t"); Serial.println(moisturemV);// Serial.print("\t"); Serial.println(vwc, 3); 
Serial.print("Cherry: "); Serial.print(moistureAnalog2); Serial.print("\t"); Serial.print(moisturemV2);// Serial.print("\t"); Serial.println(vwc2, 3); 
Serial.println();
delay(1000);
}
