/*
 * Set / get time with Seeed Studio Grove RTC
 */

#include <DS1307.h>                                         // Grove RTC
DS1307 clock; 

void setup() {
  Serial.begin(9600);
  clock.begin();
  //clock.fillByYMD(2020,11,5);                               // SET DATE AND TIME
  //clock.fillByHMS(10,31,00);
  //clock.setTime();                                          //write time to the RTC chip
}

void loop() {
  clock.getTime();
  char dateTime[40];
  sprintf(dateTime, "%04d/%02d/%02d %02d:%02d:%02d", clock.year + 2000, clock.month, clock.dayOfMonth, clock.hour, clock.minute, clock.second); 

Serial.println(dateTime);
delay(1000);
}
