/*
  Hardware:
  MKRZero
  EDT directIon nitrate and reference flow cells
  (Reference cell must be first in series, flow 
  from bottom to top to eliminate bubbling)

  Nitrate has a typical offset around 400mV at 100ppm
  and decreases 50-59mV per decade increase in concentration
  (Nernstian). 
  
  Calibrantion solution must be run through periodically 
  (eg. a few mLs weekly) with Peristaltic pump

  Pins: Reference electrode - black - GND
        NO3 - red - 5V

  https://www.chem.tamu.edu/class/fyp/stone/tutorialnotefiles/electro/nernst.htm
  https://www.horiba.com/fileadmin/uploads/Scientific/water_quality/Documents/Instruction_Manuals/Electrodes_and_Accessories/2017_Combination_ISEs/GZ0000452163_IM_E_6581S-10C_low.pdf

  Author: Leena Jaakola

  Created 30-07-2020
  
*/


#define flowcellPin A4                                       // flowcell analog output
int no3Analog;
float no3V;
float el_del = 0.059;                                        //V; -54.2mV  0C = 2.303 * R * T / (n * F)
                                                             // -56.18mV 10C
                                                             // -58.16mV 20C
                                                             // -59.16mV / decade increase in NO3 concentration (at 25C)
                                                             // -60.15mV 30C
                                                             // -62.13mV 40C
                                                             // -64.11mV 50C                                             
float no3;
float no3_ppm;

void setup() {
  Serial.begin(9600);                                             // Initialize serial monitor
  delay(100);
}

void loop() {

  no3Analog = analogRead(flowcellPin);
  no3V = no3Analog * 1000 / 1023;                                 // mV; convert the ADC reading (0 - 1023) to a voltage reading (0 - 1V):
  no3 = pow(10, ((no3V / 1000) - 0.4) / el_del);                  // Nernst equation (V) concentration mol/L
  no3_ppm = no3 * 62.0049;                                        // concentration ppm (mol/L * mol weight NO3 = g/L)
  
  delay(10);
  unsigned long mills = millis() / 1000;
  Serial.print(mills); Serial.print("\t");
  Serial.print(no3Analog); Serial.print("\t");
  Serial.print(no3V); Serial.print("\t");
  Serial.print(no3, 10); Serial.print("\t");
  Serial.print(no3_ppm, 8); Serial.println("\t");

  delay(1000);
}
