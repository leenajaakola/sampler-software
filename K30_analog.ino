/*
 *Collects CO2 concentration from K30 sensor and sends average values to Serial Monitor
 *https://www.edaphic.com.au/wp-content/uploads/2015/07/ESSE-18-K30-CO2-Sensor-Manual.pdf?x14766
 *
 *OUT2 - Pin 13
 *1-5V = 0-2000 ppm CO2
 *
 *Disable automatic baseline calibration ABC
 *calibration? 400ppm
 *error reduced <5ppm by: 
 *  individual calibration
 *  temp/pressure/RH correction
 *  average over 200s
 *  
 *cite for temp/humidity/pressure correction:   
 *  KAZAN, Filiz, "MODELING AND DEVELOPMENT OF THE DYNAMIC ENVIRONMENTAL SIMULATION 
    CHAMBER (DESC) FOR CALIBRATION OF AIR QUALITY MONITORING SENSORS" (2019). Graduate 
    Theses, Dissertations, and Problem Reports. 4089. 
    https://researchrepository.wvu.edu/etd/4089  p. 41
 *  
 *Author: Leena Jaakola
 *
 */

#define co2_Analog 13                                      

int samples = 2;//200;                           // Take the average of 200 readings
int sample_delay = 1000;                     // 1Hz
unsigned long s;

void setup() {
  Serial.begin(9600);  
  Serial.print("time(s)");
  Serial.print(", ");
  Serial.println("ppm");
}

void loop() {
  uint32_t co2_ppm = co2_average();
  s = millis();
  s = s / 1000;
  Serial.print(s);
  Serial.print("\t ");
  Serial.println(co2_ppm);
}

int co2_average() {
  uint32_t co2_sum = 0;
  for (int i = 0; i < samples; i++) {
    co2_sum += co2_single();
    delay(sample_delay);
  }
  uint32_t co2_average = co2_sum / samples;
  return co2_average;
}

int co2_single() {
  int co2_Analogread;
  int co2_mV;
  int co2_ppm = 0;
  co2_Analogread = analogRead(co2_Analog);
  co2_mV = map(co2_Analogread, 0, 1023, 1000, 5000);
  co2_ppm = map(co2_mV, 1000, 5000, 0, 2000);
  return co2_ppm;
}
